package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.TimeLineFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    ArrayList<TimeLineFeed> timeLineFeeds;
    Context context;
    public TimeLineAdapter(Context context , ArrayList<TimeLineFeed> timeLineFeeds){

     this.context = context;
     this.timeLineFeeds = timeLineFeeds;

    }
    public static class TimeLineViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.timeLineImage)
        ImageView timeLineImage;
        @BindView(R.id.timeLimePath)
        View timeLinePath;
        @BindView(R.id.timeLineStatus)
        TextView timeLineStatusText;
        @BindView(R.id.timeLineTime)
        TextView timeLineTime;
        @BindView(R.id.timeLineDescription)
        TextView timeLineDescription;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;

        public TimeLineViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_timeline_view, parent, false);
        return new TimeLineViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {

        TimeLineFeed timeLineFeed = timeLineFeeds.get(position);



        if(timeLineFeeds.size()==1){

            holder.timeLinePath.setBackgroundResource(R.drawable.bg_time_line_path_bottom_top);
        }
        else if(position==0){

            holder.timeLinePath.setBackgroundResource(R.drawable.bg_timeline_path_top);
        }
        else if(timeLineFeeds.size()-1==position){

            holder.timeLinePath.setBackgroundResource(R.drawable.bg_timeline_path_bottom);
        }

        holder.timeLineStatusText.setText(timeLineFeed.getTimeLineTitle());
        holder.timeLineTime.setText(timeLineFeed.getItemTime());

        if(!TextUtils.isEmpty(timeLineFeed.getTimeLineDescription())){

            holder.timeLineDescription.setText(timeLineFeed.getTimeLineDescription());
            holder.ratingBar.setRating(Float.valueOf(timeLineFeed.getItemRating()));
        }
        else {

            holder.timeLineDescription.setVisibility(View.GONE);
            holder.ratingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return timeLineFeeds.size();
    }



}
