package com.forsa.ejobs.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.CategoryFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.CategoryViewHolder> {


    ArrayList<CategoryFeed> categoryFeedList = new ArrayList<>();
    boolean isFromSubCategory;
    private OnItemClickListener onItemClickListener;

    public  CategoryRecyclerViewAdapter(ArrayList<CategoryFeed> categoryFeedList , boolean isFromSubCategory , OnItemClickListener onItemClickListener){

        this.categoryFeedList = categoryFeedList;
        this.isFromSubCategory = isFromSubCategory;
        this.onItemClickListener = onItemClickListener;

    }
    public static class CategoryViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.categoryTitle)
        TextView categoryTitle;
        @BindView(R.id.categoryDescription)
        TextView categoryDescription;

        public CategoryViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);

        }
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category_recycler, parent, false);
        return new CategoryViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, final int position) {

        CategoryFeed categoryFeed = categoryFeedList.get(position);

        if(!isFromSubCategory){

            holder.categoryDescription.setVisibility(View.VISIBLE);
            holder.categoryTitle.setText(categoryFeed.getCategoryTitle());
            holder.categoryDescription.setText(categoryFeed.getCategoryDescription());
        }
        else {

            holder.categoryDescription.setVisibility(View.GONE);
            holder.categoryTitle.setText(categoryFeed.getCategoryTitle());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener != null){

                    onItemClickListener.onItemClick(categoryFeedList.get(holder.getAdapterPosition()));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryFeedList.size();
    }



}
