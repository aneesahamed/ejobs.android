package com.forsa.ejobs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.HomeFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

public class SliderAdapter extends LoopingPagerAdapter<HomeFeed.SliderFeed> {

    Context context;
    ArrayList<HomeFeed.SliderFeed> sliderFeedArrayList;
    boolean isHomeSlider;
    private static final int HOME_SLIDER=0;
    private static final int SERVICE_SLIDER=1;
    public SliderAdapter(Context context, ArrayList<HomeFeed.SliderFeed> itemList, boolean isInfinite , boolean isHomeSlider) {
        super(context, itemList, isInfinite);
        this.sliderFeedArrayList=itemList;
        this.context=context;
        this.isHomeSlider=isHomeSlider;

    }

    @Override
    protected View inflateView(int viewType, ViewGroup container, int listPosition) {

        if(viewType==HOME_SLIDER){

            return   LayoutInflater.from(context).inflate(R.layout.list_item_home_slider, container, false);
        }
        else {

            return  LayoutInflater.from(context).inflate(R.layout.list_item_service_detail_slider, container, false);
        }
    }

    @Override
    protected void bindView(View convertView, int listPosition, int viewType) {


        if(sliderFeedArrayList.size()>0){

            HomeFeed.SliderFeed sliderFeed=sliderFeedArrayList.get(listPosition);


            if(viewType==HOME_SLIDER){

                TextView sliderTitle=convertView.findViewById(R.id.sliderTitle);
                TextView sliderDescription=convertView.findViewById(R.id.sliderDescription);

                sliderTitle.setText(sliderFeed.getSliderTitle());
                sliderDescription.setText(sliderFeed.getSliderDescription());
            }
            PorterShapeImageView sliderImage=convertView.findViewById(R.id.sliderImage);

            Glide.with(context).load(sliderFeed.getSliderUrl()).into(sliderImage);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(isHomeSlider){


                    }
                    else {


                    }

                }
            });

        }

    }

    @Override
    protected int getItemViewType(int listPosition) {

        return  sliderFeedArrayList.get(listPosition).isHomeSlider()? HOME_SLIDER : SERVICE_SLIDER;
    }
}
