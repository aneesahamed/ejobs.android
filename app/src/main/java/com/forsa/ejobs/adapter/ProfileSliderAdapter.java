package com.forsa.ejobs.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.ProfileFeed;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileSliderAdapter extends LoopingPagerAdapter<ProfileFeed> {

    Context context;
    ArrayList<ProfileFeed> sliderFeedArrayList;

    public ProfileSliderAdapter(Context context, ArrayList<ProfileFeed> itemList, boolean isInfinite ) {

        super(context, itemList, isInfinite);
        this.sliderFeedArrayList = itemList;
        this.context=context;
    }

    @Override
    protected View inflateView(int viewType, ViewGroup container, int listPosition) {

        return   LayoutInflater.from(context).inflate(R.layout.list_item_slider_profile, container, false);

    }

    @Override
    protected void bindView(View convertView, final int listPosition, int viewType) {

        ProfileFeed profileFeed = sliderFeedArrayList.get(listPosition);

        CircleImageView profilePic = convertView.findViewById(R.id.profilePicture);

        RelativeLayout actionEdit = convertView.findViewById(R.id.actionEdit);
        actionEdit.setVisibility(View.INVISIBLE);

        if(TextUtils.isEmpty(profileFeed.getFilePath())){

            Glide.with(context).load(R.drawable.ic_user_placeholder).placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder).into(profilePic);
        }
        else {

            Glide.with(context).load(profileFeed.getFilePath()).placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder).into(profilePic);
        }




    }


    }
