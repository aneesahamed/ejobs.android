package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.NotificationFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationListRecyclerViewAdapter extends RecyclerView.Adapter<NotificationListRecyclerViewAdapter.NotificationListHolder> {


    ArrayList<NotificationFeed> notificationFeeds;
    Context context;
    public NotificationListRecyclerViewAdapter(Context context , ArrayList<NotificationFeed> notificationFeeds){

        this.context=context;
        this.notificationFeeds=notificationFeeds;
    }

    public static class NotificationListHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.chatProfile)
        ImageView notificationProfile;
        @BindView(R.id.chatProfilePlaceHolder)
        RelativeLayout ProfilePlaceHolder;
        @BindView(R.id.placeHolderText)
        TextView placeHolderText;
        @BindView(R.id.receivedCount)
        TextView receivedCountText;
        @BindView(R.id.receivedTime)
        TextView receivedTimeText;
        @BindView(R.id.userName)
        TextView title;
        @BindView(R.id.lastMessage)
        TextView notificationContent;
        @BindView(R.id.divider)
        View divider;


        public NotificationListHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public NotificationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat, parent, false);
        return new NotificationListHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationListHolder holder, final int position) {

        NotificationFeed notificationFeed = notificationFeeds.get(position);

        holder.receivedCountText.setVisibility(View.GONE);

        holder.title.setText(notificationFeed.getTitle());
        holder.notificationContent.setText(notificationFeed.getContent());
        holder.receivedTimeText.setText(notificationFeed.getReceivedTime());

        if(notificationFeed.getProfileStatus()){

            Glide.with(context)
                    .load(notificationFeed.getUserProfileUrl())
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder).into(holder.notificationProfile);
        }
        else {

            holder.notificationProfile.setVisibility(View.INVISIBLE);
            holder.ProfilePlaceHolder.setVisibility(View.VISIBLE);
        }

        if(notificationFeeds.size()-1==position){

            holder.divider.setVisibility(View.INVISIBLE);
        }
        else {

            holder.divider.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return notificationFeeds.size();
    }


    }
