package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ProposalFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProposalListRecyclerViewAdapter extends RecyclerView.Adapter<ProposalListRecyclerViewAdapter.ProposalViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<ProposalFeed> proposalFeedList;
    Context context;
    public ProposalListRecyclerViewAdapter(Context context ,ArrayList<ProposalFeed> proposalFeedList ,OnItemClickListener onItemClickListener){


        this.proposalFeedList = proposalFeedList;
        this.context = context;
        this.onItemClickListener = onItemClickListener;

    }

    public static class  ProposalViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.proposalImage)
        CircleImageView proposalImage;
        @BindView(R.id.proposalTitle)
        TextView proposalTitle;
        @BindView(R.id.proposalAmount)
        TextView proposalAmount;

        public ProposalViewHolder(View view){

            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public ProposalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_proposals, parent, false);
        return new ProposalViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ProposalViewHolder holder, final int position) {

        ProposalFeed proposalFeed = proposalFeedList.get(position);
        holder.proposalTitle.setText(proposalFeed.getProposalName());
        holder.proposalAmount.setText(proposalFeed.getProposalPrice());

        Glide.with(context).load(proposalFeed.getProposalImageUrl())
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(holder.proposalImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener!=null){

                    onItemClickListener.onItemClick(proposalFeedList.get(holder.getAdapterPosition()));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return proposalFeedList.size();
    }


}
