package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.MyOrderFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrderListRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderListRecyclerViewAdapter.MyOrderViewHolder>{

    Context context;
    ArrayList<MyOrderFeed> myOrderFeeds = new ArrayList<>();
    private  OnItemClickListener onItemClickListener;
    public MyOrderListRecyclerViewAdapter(Context context , ArrayList<MyOrderFeed> myOrderFeeds ,OnItemClickListener onItemClickListener){

        this.context = context;
        this.myOrderFeeds = myOrderFeeds;
        this.onItemClickListener = onItemClickListener;
    }

    public static class MyOrderViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.myOrderItemImage)
        ImageView myOrderImage;
        @BindView(R.id.myOrderTitle)
        TextView myOrderTitle;
        @BindView(R.id.myOrderDescription)
        TextView myOrderDescription;
        @BindView(R.id.myOrderPrice)
        TextView myOrderPrice;
        @BindView(R.id.myOrderTime)
        TextView myOrderTime;
        @BindView(R.id.divider)
        View divider;

        public MyOrderViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public MyOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_order, parent, false);
        return new MyOrderViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyOrderViewHolder holder, final int position) {

        MyOrderFeed myOrderFeed = myOrderFeeds.get(position);

        holder.myOrderTitle.setText(myOrderFeed.getMyOrderName());
        holder.myOrderDescription.setText(myOrderFeed.getMyOrderDescription());
        holder.myOrderTime.setText(myOrderFeed.getMyOrderTime());
        holder.myOrderPrice.setText(myOrderFeed.getMyOrderPrice());

        Glide.with(context).load(myOrderFeed.getImageUrl())
                .placeholder(R.drawable.place_holder_image)
                .error(R.drawable.place_holder_image)
                .into(holder.myOrderImage);

        if(myOrderFeeds.size()-1==position){

            holder.divider.setVisibility(View.INVISIBLE);
        }
        else {
            holder.divider.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener != null){

                    onItemClickListener.onItemClick(myOrderFeeds.get(holder.getAdapterPosition()));

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return myOrderFeeds.size();
    }

    }
