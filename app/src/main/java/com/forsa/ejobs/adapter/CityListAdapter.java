package com.forsa.ejobs.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.CityFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityViewHolder> {

    ArrayList<CityFeed> cityList;

    public CityListAdapter(ArrayList<CityFeed> cityList){

        this.cityList = cityList;
    }
    public static class CityViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.cityListName)
        TextView cityListName;
        @BindView(R.id.cityListCheckBox)
        CheckBox cityListCheckBox;

        public CityViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_city_list, parent, false);

        return new CityViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final CityViewHolder holder, final int position) {

        final CityFeed cityFeed = cityList.get(position);

        holder.cityListName.setText(cityFeed.getCityName());

        holder.cityListCheckBox.setChecked(cityFeed.isSelected());

        holder.cityListCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.cityListCheckBox.setChecked(!cityFeed.isSelected());
                cityList.get(position).setSelected(!cityFeed.isSelected());


            }
        });
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }


}
