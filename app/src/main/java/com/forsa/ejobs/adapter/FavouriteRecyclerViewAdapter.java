package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.FavouriteFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteRecyclerViewAdapter extends RecyclerView.Adapter<FavouriteRecyclerViewAdapter.FavouriteViewHolder>{



    Context context;
    String currencySymbol;
    ArrayList<FavouriteFeed> favouriteFeeds;
    public FavouriteRecyclerViewAdapter(Context context , ArrayList<FavouriteFeed> favouriteFeeds) {

        this.context = context;
        this.favouriteFeeds = favouriteFeeds;
        this.currencySymbol =context.getString(R.string.currencySymbol);

    }
    public static class FavouriteViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.serviceListItemImage)
        PorterShapeImageView itemImage;
        @BindView(R.id.ratingStarImage)
        ImageView ratingStarImage;
        @BindView(R.id.ratingText)
        TextView ratingText;
        @BindView(R.id.ratingCountText)
        TextView ratingCountText;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.itemTime)
        TextView itemPrice;
        @BindView(R.id.itemFavouriteImage)
        ImageView itemFavouriteImage;
       public FavouriteViewHolder(View view){

           super(view);
           ButterKnife.bind(this , view);
       }

    }

    @Override
    public FavouriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_service, parent, false);
        return new FavouriteViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final FavouriteViewHolder holder, final int position) {

        FavouriteFeed favouriteFeed = favouriteFeeds.get(position);

        holder.ratingText.setText(favouriteFeed.getRating());
        holder.ratingCountText.setText("("+favouriteFeed.getRatingCount()+")");
        holder.description.setText(favouriteFeed.getDescription());
        holder.itemPrice.setText(currencySymbol +favouriteFeed.getPrice());
        holder.itemFavouriteImage.setVisibility(View.INVISIBLE);

        Glide.with(context).load(favouriteFeed.getImageUrl())
                .placeholder(R.drawable.place_holder_image)
                .error(R.drawable.place_holder_image)
                .into(holder.itemImage);

    }
    @Override
    public int getItemCount() {
        return favouriteFeeds.size();
    }

    }
