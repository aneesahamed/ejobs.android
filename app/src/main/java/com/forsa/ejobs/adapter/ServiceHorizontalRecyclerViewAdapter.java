package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.ServiceFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceHorizontalRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<ServiceFeed> serviceFeedList;
    Context context;
    String currencySymbol;

    public static final int PROMOTED_ITEM_SERVICE = 0;
    public static final int RECENTLY_VIEWED_SERVICE = 1;

    public ServiceHorizontalRecyclerViewAdapter(Context context , ArrayList<ServiceFeed> serviceFeedList){

        this.context=context;
        this.serviceFeedList=serviceFeedList;
        this.currencySymbol =context.getString(R.string.currencySymbol);

    }

    public static class ServiceFeedViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.itemImage)
        PorterShapeImageView itemImage;
        @BindView(R.id.imageFavourite)
        ImageView imageFavourite;
        @BindView(R.id.itemTitle)
        TextView itemTitle;
        @BindView(R.id.servicePrice)
        TextView itemPrice;
        @BindView(R.id.ratingText)
        TextView ratingText;


        public ServiceFeedViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public static class RecentServiceFeedViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.itemImage)
        PorterShapeImageView itemImage;
        @BindView(R.id.imageFavourite)
        ImageView imageFavourite;
        @BindView(R.id.itemTitle)
        TextView itemTitle;
        @BindView(R.id.servicePrice)
        TextView itemPrice;
        @BindView(R.id.ratingText)
        TextView ratingText;


        public RecentServiceFeedViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view;

        if(viewType==PROMOTED_ITEM_SERVICE){

           view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_promoted_recycler, parent, false);
            return new ServiceFeedViewHolder(view);
        }
        else if(viewType==RECENTLY_VIEWED_SERVICE) {

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_recentview_recycler, parent, false);
            return new RecentServiceFeedViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        ServiceFeed serviceFeed=serviceFeedList.get(position);

        if(holder instanceof ServiceFeedViewHolder){

            ((ServiceFeedViewHolder)holder).itemTitle.setText(serviceFeed.getServiceName());
            ((ServiceFeedViewHolder)holder).itemPrice.setText(currencySymbol +serviceFeed.getServicePrice());
            ((ServiceFeedViewHolder)holder).ratingText.setText(serviceFeed.getServiceRating());

            Glide.with(context).load(serviceFeed.getServiceImageUrl()).placeholder(R.drawable.place_holder_image).error(R.drawable.place_holder_image).into(((ServiceFeedViewHolder)holder).itemImage);

            if(serviceFeed.getIsFavouriteService()){

                ((ServiceFeedViewHolder)holder).imageFavourite.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
            }
            else {

                ((ServiceFeedViewHolder)holder).imageFavourite.setColorFilter(ContextCompat.getColor(context,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);

            }
        }
        else if(holder instanceof RecentServiceFeedViewHolder) {

            ((RecentServiceFeedViewHolder)holder).itemTitle.setText(serviceFeed.getServiceName());
            ((RecentServiceFeedViewHolder)holder).itemPrice.setText(currencySymbol +serviceFeed.getServicePrice());
            ((RecentServiceFeedViewHolder)holder).ratingText.setText(serviceFeed.getServiceRating());

            Glide.with(context).load(serviceFeed.getServiceImageUrl()).placeholder(R.drawable.place_holder_image).error(R.drawable.place_holder_image).into(((RecentServiceFeedViewHolder)holder).itemImage);

            if(serviceFeed.getIsFavouriteService()){

                ((RecentServiceFeedViewHolder)holder).imageFavourite.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
            }
            else {

                ((RecentServiceFeedViewHolder)holder).imageFavourite.setColorFilter(ContextCompat.getColor(context,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);

            }

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

    }

    @Override
    public int getItemCount() {
        return serviceFeedList.size();
    }

    @Override
    public int getItemViewType(int position) {

        return serviceFeedList.get(position).getIsRecentService()? RECENTLY_VIEWED_SERVICE : PROMOTED_ITEM_SERVICE;
    }
}
