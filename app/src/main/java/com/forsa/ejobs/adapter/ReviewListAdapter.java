package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.ReviewFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewViewHolder>{



    ArrayList<ReviewFeed> reviewFeeds;
    Context context;
    public ReviewListAdapter(Context context , ArrayList<ReviewFeed> reviewFeeds){

        this.context = context;
        this.reviewFeeds = reviewFeeds;

    }

    public static class ReviewViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.profile)
        CircleImageView profileImage;
        @BindView(R.id.ProfilePlaceHolder)
        RelativeLayout placeHolderLayout;
        @BindView(R.id.placeHolderText)
        TextView placeHolderText;
        @BindView(R.id.reviewUsername)
        TextView reviewerName;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.reviewDescription)
        TextView reviewDescription;
        @BindView(R.id.reviewTime)
        TextView reviewTime;

        public ReviewViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reviews, parent, false);
        return new ReviewViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ReviewViewHolder holder, final int position) {

        ReviewFeed reviewFeed = reviewFeeds.get(position);

        holder.placeHolderLayout.setVisibility(View.VISIBLE);
        holder.reviewerName.setText(reviewFeed.getReviewerName());
        holder.ratingBar.setRating(reviewFeed.getRating());
        holder.reviewDescription.setText(reviewFeed.getReviewerDescription());
        holder.reviewTime.setText(reviewFeed.getRatingTime());

        holder.profileImage.setVisibility(View.INVISIBLE);


    }


    @Override
    public int getItemCount() {
        return reviewFeeds.size();
    }




}
