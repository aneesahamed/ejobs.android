package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.ChatFeed;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class ChatThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int SENDER_TYPE=0;
    private static final int RECEIVER_TYPE=1;

    ArrayList<ChatFeed> threadList;
    Context context;
    public ChatThreadAdapter(Context context , ArrayList<ChatFeed> threadList){

        this.context = context;
        this.threadList = threadList;

    }
    public static class SenderViewHolder extends RecyclerView.ViewHolder{

        public SenderViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public static class ReceiverViewHolder extends RecyclerView.ViewHolder{

        public ReceiverViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if(viewType==SENDER_TYPE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_bubble_left, parent, false);
            return new SenderViewHolder(view);
        }
        else if (viewType==RECEIVER_TYPE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_bubble_right, parent, false);
            return new ReceiverViewHolder(view);
        }

        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof SenderViewHolder){

        }

        else if (holder instanceof ReceiverViewHolder){


        }
    }

    @Override
    public int getItemCount() {
        return threadList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return threadList.get(position).isSender()?SENDER_TYPE:RECEIVER_TYPE;
    }
}
