package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ChatListFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListRecyclerViewAdapter extends RecyclerView.Adapter<ChatListRecyclerViewAdapter.ChatListHolder> {


    ArrayList<ChatListFeed> chatList;
    Context context;
    OnItemClickListener onItemClickListener;
    public ChatListRecyclerViewAdapter(Context context , ArrayList<ChatListFeed> chatList , OnItemClickListener onItemClickListener){


        this.chatList=chatList;
        this.context=context;
        this.onItemClickListener = onItemClickListener;
    }

    public static class ChatListHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.chatProfile)
        ImageView chatProfile;
        @BindView(R.id.chatProfilePlaceHolder)
        RelativeLayout chatProfilePlaceHolder;
        @BindView(R.id.placeHolderText)
        TextView placeHolderText;
        @BindView(R.id.receivedCount)
        TextView receivedCountText;
        @BindView(R.id.receivedTime)
        TextView receivedTimeText;
        @BindView(R.id.userName)
        TextView userName;
        @BindView(R.id.lastMessage)
        TextView recentMessage;
        @BindView(R.id.divider)
        View divider;


        public ChatListHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }


    @Override
    public ChatListHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat, parent, false);
        return new ChatListHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatListHolder holder, final int position) {

        ChatListFeed chatListFeed = chatList.get(position);

        holder.userName.setText(chatListFeed.getUserName());
        holder.recentMessage.setText(chatListFeed.getRecentMessage());
        holder.receivedTimeText.setText(chatListFeed.getReceivedTime());
        holder.receivedCountText.setText(String.valueOf(chatListFeed.getUnReadCount()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener!=null){

                    onItemClickListener.onItemClick(chatList.get(holder.getAdapterPosition()));
                }
            }
        });

        if(chatListFeed.getProfileStatus()){

            holder.chatProfilePlaceHolder.setVisibility(View.INVISIBLE);
            Glide.with(context).load(chatListFeed.getUserProfileUrl())
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .into(holder.chatProfile);

        }
        else {

            holder.chatProfile.setVisibility(View.INVISIBLE);
            holder.chatProfilePlaceHolder.setVisibility(View.VISIBLE);

        }

        if(chatList.size()-1==position){
            holder.divider.setVisibility(View.INVISIBLE);
        }
        else {
            holder.divider.setVisibility(View.VISIBLE);
        }



    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }



    }
