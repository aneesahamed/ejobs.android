package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ServiceFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceListRecyclerViewAdapter extends RecyclerView.Adapter<ServiceListRecyclerViewAdapter.ServiceListHolder> {

    Context context;
    String currencySymbol;
    ArrayList<ServiceFeed> serviceFeedList;
    private OnItemClickListener onItemClickListener;

    public ServiceListRecyclerViewAdapter(Context context , ArrayList<ServiceFeed> serviceFeedList , OnItemClickListener onItemClickListener){

        this.context = context;
        this.currencySymbol =context.getString(R.string.currencySymbol);
        this.serviceFeedList = serviceFeedList;
        this.onItemClickListener = onItemClickListener;
    }

    public static class ServiceListHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.serviceListItemImage)
        PorterShapeImageView itemImage;
        @BindView(R.id.ratingStarImage)
        ImageView ratingStarImage;
        @BindView(R.id.ratingText)
        TextView ratingText;
        @BindView(R.id.ratingCountText)
        TextView ratingCountText;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.itemTime)
        TextView itemPrice;
        @BindView(R.id.itemFavouriteImage)
        ImageView itemFavouriteImage;
        public ServiceListHolder(View view){

            super(view);
            ButterKnife.bind(this,view);

        }
    }

    @Override
    public ServiceListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_service, parent, false);
        return new ServiceListHolder(view);
    }

    @Override
    public void onBindViewHolder(final ServiceListHolder holder, final int position) {

        ServiceFeed serviceFeed = serviceFeedList.get(position);

        holder.description.setText(serviceFeed.getServiceDescription());
        holder.ratingText.setText(serviceFeed.getServiceRating());
        holder.ratingCountText.setText("("+serviceFeed.getServiceCount()+")");
        holder.itemPrice.setText(currencySymbol+serviceFeed.getServicePrice());

        Glide.with(context).load(serviceFeed.getServiceImageUrl())
                .placeholder(R.drawable.place_holder_image)
                .error(R.drawable.place_holder_image)
                .into(holder.itemImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener != null){

                    onItemClickListener.onItemClick(serviceFeedList.get(holder.getAdapterPosition()));

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return serviceFeedList.size();
    }

    }
