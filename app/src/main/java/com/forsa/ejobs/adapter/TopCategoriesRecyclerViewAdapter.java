package com.forsa.ejobs.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.CategoryFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopCategoriesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<CategoryFeed> categoryFeedList;
    OnItemClickListener onItemClickListener;
    public TopCategoriesRecyclerViewAdapter(ArrayList<CategoryFeed> categoryFeedList , OnItemClickListener onItemClickListener){

        this.categoryFeedList=categoryFeedList;
        this.onItemClickListener = onItemClickListener;

    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.categoryTitle)
        TextView categoryTitle;
        @BindView(R.id.categoryDescription)
        TextView categoryDescription;
        @BindView(R.id.iconRightArrow)
        ImageView rightArrow;
        @BindView(R.id.dividerLine)
        View dividerLine;
        public CategoryViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category_recycler, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        CategoryFeed categoryFeed=categoryFeedList.get(position);

        ((CategoryViewHolder)holder).categoryTitle.setText(categoryFeed.getCategoryTitle());
        ((CategoryViewHolder)holder).categoryDescription.setText(categoryFeed.getCategoryDescription());
        ((CategoryViewHolder)holder).rightArrow.setVisibility(View.INVISIBLE);

        ((CategoryViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener !=null){

                    onItemClickListener.onItemClick(categoryFeedList.get(holder.getAdapterPosition()));
                }
            }
        });

        if(categoryFeedList.size()-1==position){

            ((CategoryViewHolder)holder).dividerLine.setVisibility(View.INVISIBLE);
        }
        else {

            ((CategoryViewHolder)holder).dividerLine.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return categoryFeedList.size();
    }


}
