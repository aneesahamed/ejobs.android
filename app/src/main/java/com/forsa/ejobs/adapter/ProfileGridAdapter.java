package com.forsa.ejobs.adapter;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ProfileFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileGridAdapter extends RecyclerView.Adapter<ProfileGridAdapter.ProfileViewHolder> {


    Context context;
    ArrayList<ProfileFeed> filePathList;
    OnItemClickListener onItemClickListener;

    public ProfileGridAdapter(Context context ,ArrayList<ProfileFeed> filePathList ,OnItemClickListener onItemClickListener){

        this.context = context;
        this.filePathList = filePathList;
        this.onItemClickListener = onItemClickListener;

    }
    public static class ProfileViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.profileLayout)
        RelativeLayout profileLayout;
        @BindView(R.id.selectedProfilePicture)
        PorterShapeImageView selectedPicture;
        @BindView(R.id.actionCancel)
        RelativeLayout actionCancel;
        @BindView(R.id.actionAddMore)
        FloatingActionButton actionAddMore;

        public ProfileViewHolder(View view){
            super(view);
            ButterKnife.bind(this ,view);
        }
    }

    @Override
    public ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_profile_picture, parent, false);
        return new ProfileViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ProfileViewHolder holder, final int position) {

        ProfileFeed profileFeed = filePathList.get(position);

        if(!profileFeed.isAddItem()){

                holder.profileLayout.setBackgroundResource(0);
                holder.actionCancel.setVisibility(View.VISIBLE);
                holder.actionAddMore.setVisibility(View.INVISIBLE);
                holder.selectedPicture.setVisibility(View.VISIBLE);
                String path = profileFeed.getFilePath();
                if(path.startsWith("file:"))
                    Glide.with(context).load(new File(path)).into(holder.selectedPicture);
                else
                    Glide.with(context).load(path).into(holder.selectedPicture);

            }
            else {

            holder.profileLayout.setBackgroundResource(R.drawable.bg_profile_add_item);
            holder.actionCancel.setVisibility(View.INVISIBLE);
            holder.actionAddMore.setVisibility(View.VISIBLE);
            holder.selectedPicture.setVisibility(View.INVISIBLE);
            holder.itemView.setVisibility(View.VISIBLE);

        }

        if(filePathList.size()==6){

            if(filePathList.size()-1==position){


                holder.itemView.setVisibility(View.INVISIBLE);
            }
        }

        holder.actionAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener!=null){

                    onItemClickListener.onItemClick(null);

                }

            }
        });

        holder.actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filePathList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });



    }

    public ArrayList<ProfileFeed> getFilePathList(){

        return  new ArrayList<>(this.filePathList);
    }

    @Override
    public int getItemCount() {
        return filePathList.size();
    }

}
