package com.forsa.ejobs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.RequestFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyRequestListAdapter extends RecyclerView.Adapter<MyRequestListAdapter.MyRequestViewHolder> {



    Context context;
    ArrayList<RequestFeed> requestFeeds;
    OnItemClickListener onItemClickListener;
    String currencySymbol;
    public MyRequestListAdapter(Context context , ArrayList<RequestFeed> requestFeeds , OnItemClickListener onItemClickListener){

        this.requestFeeds = requestFeeds;
        this.onItemClickListener = onItemClickListener;
        this.currencySymbol = context.getString(R.string.currencySymbol);

    }
    public static class MyRequestViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.myRequestDescription)
        TextView myRequestDescription;
        @BindView(R.id.offerValue)
        TextView offerValue;
        @BindView(R.id.budgetValue)
        TextView budgetValue;
        @BindView(R.id.requestDate)
        TextView requestDate;
        public MyRequestViewHolder(View view){

            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public MyRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_request, parent, false);
        return new MyRequestViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MyRequestViewHolder holder, final int position) {

        RequestFeed requestFeed = requestFeeds.get(position);

        holder.myRequestDescription.setText(requestFeed.getRequestDescription());
        holder.offerValue.setText("("+requestFeed.getRequestOffer()+")");
        holder.budgetValue.setText(currencySymbol+requestFeed.getRequestBudget());
        holder.requestDate.setText(requestFeed.getRequestDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(onItemClickListener!=null){

                    onItemClickListener.onItemClick(requestFeeds.get(holder.getAdapterPosition()));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return requestFeeds.size();
    }


}
