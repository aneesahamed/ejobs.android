package com.forsa.ejobs.model.retrofit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.forsa.ejobs.BuildConfig;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.entity.DeviceInfo;
import com.forsa.ejobs.model.entity.User;


import java.util.HashMap;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager {

    public interface ResponseListener {
        void onSuccess(Object object);

        void onFailure(Object object);
    }

    private Retrofit retrofit = null;
    private static RetrofitManager instance;
    private Context context;
    static OkHttpClient client;
    public static RetrofitEndpoints api;

    public static synchronized RetrofitManager getInstance(Context context) {
        if (instance == null) {

            instance = new RetrofitManager(context);
        }
        return instance;
    }

    private RetrofitManager(Context context) {
        this.context = context;
        retrofit = getRetrofitInstance();
        api = retrofit.create(RetrofitEndpoints.class);
    }

    public Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(RetrofitManager.createInterceptor())
                    .build();
        }
        return retrofit;
    }


    public interface Headers {
        String CONTENT_TYPE = "Content-Type:application/json";
        String USER_AGENT = "User-Agent:eJobs.App.Android";
    }

    public interface EndPoints {
        String CITIES = "city";
        String REGISTER = "register";
        String LOGIN = "login";
        String PROFILE_UPLOAD = "profile";
        String POST_REQUEST = "request";
        String GET_CATEGORY = "categories/{id}";
    }

    public interface RestParams {
        String USER_TYPE = "type";
        String PASSWORD = "p";
        String USER_NAME = "n";
        String EMAIL = "e";
        String MOBILE_NO = "m";
        String DEVICE_NAME = "dname";
        String DEVICE_ID = "did";
        String OS_VERSION = "osver";
        String APP_VERSION = "appver";
        String SOURCE = "source";
        String LOCALE = "locale";
        String CITY = "city";
        String FCM_TOKEN = "t";
        String NAME = "n";
        String PROFILE_PIC = "pic";
        String DESCRIPTION = "des";
        String CATEGORY_ID = "categoryid";
        String DELIVERY = "deliver";
        String BUDGET = "budget";
    }

    public static OkHttpClient createInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .retryOnConnectionFailure(true)
                .build();
        return client;
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void getCities(ResponseListener listener) {
        makeNetworkCall(api.getCities(), listener);
    }

    public void getCategories(String authKey, String id, ResponseListener listener) {
        makeNetworkCall(api.getCategories(authKey, id), listener);
    }

    public void userSignUp(String userName, String email, String phoneNo, String password, String fcmToken, String cityId, String uType, ResponseListener listener) {
        DeviceInfo deviceInfo = Utils.getDeviceInfo(context);
        HashMap<String, String> params = new HashMap<>();
        params.put(RestParams.USER_TYPE, uType);
        params.put(RestParams.PASSWORD, password);
        params.put(RestParams.USER_NAME, userName);
        params.put(RestParams.EMAIL, email);
        params.put(RestParams.MOBILE_NO, phoneNo);
        params.put(RestParams.DEVICE_ID, deviceInfo.getDeviceId());
        params.put(RestParams.DEVICE_NAME, deviceInfo.getDeviceName());
        params.put(RestParams.OS_VERSION, deviceInfo.getOsVersion());
        params.put(RestParams.APP_VERSION, deviceInfo.getAppVersion());
        params.put(RestParams.SOURCE, deviceInfo.getOsType());
        params.put(RestParams.LOCALE, Locale.getDefault().getLanguage());
        params.put(RestParams.CITY, cityId);
        params.put(RestParams.FCM_TOKEN, fcmToken);
        makeNetworkCall(api.register(params), listener);
    }

    public void userLogin(String email, String password, String fcmToken, ResponseListener listener) {
        DeviceInfo deviceInfo = Utils.getDeviceInfo(context);
        HashMap<String, String> params = new HashMap<>();
        params.put(RestParams.PASSWORD, password);
        params.put(RestParams.EMAIL, email);
        params.put(RestParams.DEVICE_NAME, deviceInfo.getDeviceName());
        params.put(RestParams.DEVICE_ID, deviceInfo.getDeviceId());
        params.put(RestParams.OS_VERSION, deviceInfo.getOsVersion());
        params.put(RestParams.APP_VERSION, deviceInfo.getAppVersion());
        params.put(RestParams.SOURCE, deviceInfo.getOsType());
        params.put(RestParams.LOCALE, Locale.getDefault().getLanguage());
        params.put(RestParams.FCM_TOKEN, fcmToken);
        makeNetworkCall(api.login(params), listener);
    }

    public void profileUpload(String authKey, String name, String cityId, String phoneNo, String profilePicUrl, ResponseListener listener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(RestParams.NAME, name);
        params.put(RestParams.MOBILE_NO, phoneNo);
        params.put(RestParams.CITY, cityId);
        params.put(RestParams.PROFILE_PIC, profilePicUrl);
        makeNetworkCall(api.profileUpload(authKey, params), listener);
    }

    public void postRequest(String authKey, String desc, int categoryId, int delivery, int budget, ResponseListener listener) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(RestParams.DESCRIPTION, desc);
        params.put(RestParams.CATEGORY_ID, categoryId);
        params.put(RestParams.DELIVERY, delivery);
        params.put(RestParams.BUDGET, budget);
        makeNetworkCall(api.postRequest(authKey, params), listener);
    }

    public <T> void makeNetworkCall(Call<T> call, final ResponseListener listener) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.body() != null) {
                    listener.onSuccess(response.body());
                } else if (response.errorBody() != null) {
                    listener.onFailure(new ErrorResponse(response.code(), response.message()));
                }

            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                listener.onFailure(new ErrorResponse(t.getMessage()));
            }
        });

    }

    public void cancelRequest() {
        if (client != null) {
            client.dispatcher().cancelAll();
        }
    }
}
