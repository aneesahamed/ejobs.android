package com.forsa.ejobs.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestFeed {


    String requestDescription;
    String requestOffer;
    String requestBudget;
    String requestDate;

    public void setRequestBudget(String requestBudget) {
        this.requestBudget = requestBudget;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public void setRequestOffer(String requestOffer) {
        this.requestOffer = requestOffer;

    }

    public String getRequestBudget() {
        return requestBudget;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public String getRequestOffer() {
        return requestOffer;
    }


}

