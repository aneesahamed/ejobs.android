package com.forsa.ejobs.model.entity;

public class ProfileFeed {

    public  String filePath;
    boolean isAddItem;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setIsAddItem(boolean addItem) {
        isAddItem = addItem;
    }

    public String getFilePath() {
        return filePath;
    }

    public boolean isAddItem(){

        return isAddItem;
    }
}
