package com.forsa.ejobs.model;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    private static final String USER_PREFERENCE = "user.preference";

    public interface PrefKey{
        String USER = "user";
        String FCM_TOKEN = "fcmToken";
    }
    private static SharedPreferences getUserSharedPreference(Context context) {
        return context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
    }

    public static void clearPreferenceString(Context context, String key){
        SharedPreferences.Editor editor = getUserSharedPreference(context).edit();
        editor.remove(key);
        editor.apply();
    }

    public static String getString(Context context, String name, String defValue) {
        return getUserSharedPreference(context).getString(name, defValue);
    }

    public static void setString(Context context, String name, String value) {
        SharedPreferences.Editor editor = getUserSharedPreference(context).edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static int getInt(Context context, String name, int defValue) {
        return getUserSharedPreference(context).getInt(name, defValue);
    }

    public static void setInt(Context context, String name, int value) {
        SharedPreferences.Editor editor = getUserSharedPreference(context).edit();
        editor.putInt(name, value);
        editor.apply();
    }
}
