package com.forsa.ejobs.model.retrofit;

import com.forsa.ejobs.model.entity.BaseResponse;
import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.model.entity.PostRequestFeed;
import com.forsa.ejobs.model.entity.User;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitEndpoints {

    @Headers({RetrofitManager.Headers.CONTENT_TYPE,RetrofitManager.Headers.USER_AGENT})
    @GET(RetrofitManager.EndPoints.CITIES)
    Call<CityFeed> getCities();

    @Headers({RetrofitManager.Headers.CONTENT_TYPE,RetrofitManager.Headers.USER_AGENT})
    @POST(RetrofitManager.EndPoints.REGISTER)
    Call<User> register(@Body HashMap<String ,String> params);

    @Headers({RetrofitManager.Headers.CONTENT_TYPE,RetrofitManager.Headers.USER_AGENT})
    @POST(RetrofitManager.EndPoints.LOGIN)
    Call<User> login(@Body HashMap<String ,String> params);

    @Headers({RetrofitManager.Headers.CONTENT_TYPE,RetrofitManager.Headers.USER_AGENT})
    @POST(RetrofitManager.EndPoints.PROFILE_UPLOAD)
    Call<BaseResponse> profileUpload(@Header ("Authorization")String authKey, @Body HashMap<String ,String> params);

    @Headers({RetrofitManager.Headers.CONTENT_TYPE, RetrofitManager.Headers.USER_AGENT})
    @POST(RetrofitManager.EndPoints.POST_REQUEST)
    Call<BaseResponse> postRequest(@Header ("Authorization")String authKey, @Body HashMap<String, Object> params);

    @Headers({RetrofitManager.Headers.CONTENT_TYPE, RetrofitManager.Headers.USER_AGENT})
    @GET(RetrofitManager.EndPoints.GET_CATEGORY)
    Call<PostRequestFeed> getCategories(@Header("Authorization") String authKey, @Path("id") String id);
}
