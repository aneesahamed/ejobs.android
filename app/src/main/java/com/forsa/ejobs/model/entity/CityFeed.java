package com.forsa.ejobs.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityFeed {

    @SerializedName("cityFeed")
    List<City> cityList;
    @SerializedName("success")
    int success;

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getSuccess() {
        return success;
    }

    public List<City> getCityList() {
        return cityList;
    }

    private String cityName;
    private boolean isSelected;

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCityName() {
        return cityName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public class City {
        @SerializedName("id")
        String id;
        @SerializedName("name")
        String name;

        public String getName() {
            return name;
        }

        public String getId() {
            return id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
