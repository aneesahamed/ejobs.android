package com.forsa.ejobs.model.entity;

public class ChatFeed {

    private long timeStamp;
    private int messageType;
    private int mediaType;
    private String messageContent;
    private String fileName;
    private boolean isSender;

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public void setIsSender(boolean sender) {
        isSender = sender;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMediaType() {
        return mediaType;
    }

    public int getMessageType() {
        return messageType;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public boolean isSender(){
        return isSender;
    }
}
