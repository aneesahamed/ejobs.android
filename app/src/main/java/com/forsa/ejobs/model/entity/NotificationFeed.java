package com.forsa.ejobs.model.entity;

public class NotificationFeed {


    public String title;
    public String content;
    public String receivedTime;
    public String userProfileUrl;
    public boolean profileStatus;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public void setUserProfileUrl(String userProfileUrl) {
        this.userProfileUrl = userProfileUrl;
    }

    public void setProfileStatus(boolean profileStatus){

        this.profileStatus=profileStatus;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public String getUserProfileUrl() {
        return userProfileUrl;
    }

    public boolean getProfileStatus() {
        return profileStatus;
    }
}
