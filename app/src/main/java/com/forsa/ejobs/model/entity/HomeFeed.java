package com.forsa.ejobs.model.entity;


import java.util.ArrayList;

public class HomeFeed {

    public ArrayList<SliderFeed> sliderFeedList;
    public ArrayList<CategoryFeed> categoryFeedList;
    public ArrayList<ServiceFeed> promotedItemFeedList;
    public ArrayList<ServiceFeed> recentlyItemFeedList;

    public void setSliderFeedList(ArrayList<SliderFeed> sliderFeedList) {
        this.sliderFeedList = sliderFeedList;
    }

    public void setCategoryFeedList(ArrayList<CategoryFeed> categoryFeedList) {

        this.categoryFeedList = categoryFeedList;
    }

    public void setPromotedItemFeedList(ArrayList<ServiceFeed> promotedItemFeedList) {

        this.promotedItemFeedList = promotedItemFeedList;
    }

    public void setRecentlyItemFeedList(ArrayList<ServiceFeed> recentlyItemFeedList) {

        this.recentlyItemFeedList = recentlyItemFeedList;
    }

    public ArrayList<SliderFeed> getSliderFeedList() {
        return sliderFeedList;
    }

    public ArrayList<CategoryFeed> getCategoryFeedList() {
        return categoryFeedList;
    }

    public ArrayList<ServiceFeed> getPromotedItemFeedList() {
        return promotedItemFeedList;
    }

    public ArrayList<ServiceFeed> getRecentlyItemFeedList() {
        return recentlyItemFeedList;
    }

    public static class SliderFeed {

        String sliderUrl;
        String sliderTitle;
        String sliderDescription;
        boolean isForHomeSlider;

        public void setSliderUrl(String sliderUrl){

            this.sliderUrl=sliderUrl;
        }

        public void setSliderTitle(String sliderTitle){

            this.sliderTitle=sliderTitle;
        }

        public void  setSliderDescription(String sliderDescription){

            this.sliderDescription=sliderDescription;
        }

        public String getSliderUrl(){
            return sliderUrl;
        }

        public String getSliderDescription() {

            return sliderDescription;
        }

        public String getSliderTitle() {
            return sliderTitle;
        }

        public void setIsForHomeSlider(boolean forHomeSlider) {
            isForHomeSlider = forHomeSlider;
        }

        public boolean isHomeSlider(){

            return isForHomeSlider;
        }
    }



}
