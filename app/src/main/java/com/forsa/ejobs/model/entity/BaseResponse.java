package com.forsa.ejobs.model.entity;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("success")
    int success;

    public int success() { return success; }
}
