package com.forsa.ejobs.model.entity;

public class TimeLineFeed {

public String imageUrl;
public String timeLineTitle;
public String timeLineDescription;
public String itemTime;
public String itemRating;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setTimeLineTitle(String timeLineTitle) {
        this.timeLineTitle = timeLineTitle;
    }

    public void setItemRating(String itemRating) {
        this.itemRating = itemRating;
    }

    public void setItemTime(String itemTime) {
        this.itemTime = itemTime;
    }

    public void setTimeLineDescription(String timeLineDescription) {
        this.timeLineDescription = timeLineDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getItemRating() {
        return itemRating;
    }

    public String getItemTime() {
        return itemTime;
    }

    public String getTimeLineDescription() {
        return timeLineDescription;
    }

    public String getTimeLineTitle() {
        return timeLineTitle;
    }
}
