package com.forsa.ejobs.model.entity;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Locale;

public class PostRequestFeed {

    @SerializedName("categoryFeed")
    List<PostRequestFeed.Category> categoryList;
    @SerializedName("success")
    int success;

    public void setCategoryList(List<Category> cityList) {
        this.categoryList = categoryList;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getSuccess() {
        return success;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }
    public class Category {
        @SerializedName("categoryId")
        String categoryId;
        @SerializedName("categoryNameEn")
        String categoryNameEn;
        @SerializedName("categoryNameAr")
        String categoryNameAr;
        @SerializedName("categoryImage")
        String categoryImage;

        public String getCategoryName(){
            if(Locale.getDefault().getLanguage().equals("ar")){
                return getCategoryNameAr();
            }
            else {
                return getCategoryNameEn();
            }

        }
        public String getCategoryId() {
            return categoryId;
        }

        private String getCategoryNameEn() {
            return categoryNameEn;
        }

        private String getCategoryNameAr() {
            return categoryNameAr;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public void setCategoryNameEn(String categoryNameEn) {
            this.categoryNameEn = categoryNameEn;
        }

        public void setCategoryNameAr(String categoryNameAr) {
            this.categoryNameAr = categoryNameAr;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }
    }
}
