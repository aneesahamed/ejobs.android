package com.forsa.ejobs.model.entity;

import org.parceler.Parcel;

@Parcel
public class CategoryFeed {

    public String categoryTitle;
    public String categoryDescription;

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public void setCategoryTitle(String categoryTitle){

        this.categoryTitle=categoryTitle;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }
}
