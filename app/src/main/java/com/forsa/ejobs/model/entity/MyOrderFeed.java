package com.forsa.ejobs.model.entity;

public class MyOrderFeed {

    public String imageUrl;
    public String myOrderName;
    public String myOrderDescription;
    public String myOrderTime;
    public String myOrderPrice;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setMyOrderName(String myOrderName) {
        this.myOrderName = myOrderName;
    }

    public void setMyOrderDescription(String myOrderDescription) {
        this.myOrderDescription = myOrderDescription;
    }

    public void setMyOrderTime(String myOrderTime) {
        this.myOrderTime = myOrderTime;
    }

    public void setMyOrderPrice(String myOrderPrice) {
        this.myOrderPrice = myOrderPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMyOrderName() {
        return myOrderName;
    }

    public String getMyOrderDescription() {
        return myOrderDescription;
    }

    public String getMyOrderTime() {
        return myOrderTime;
    }

    public String getMyOrderPrice() {
        return myOrderPrice;
    }
}
