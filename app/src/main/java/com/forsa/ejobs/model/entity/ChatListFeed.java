package com.forsa.ejobs.model.entity;

public class ChatListFeed {

    public String userName;
    public String recentMessage;
    public int unReadCount;
    public String receivedTime;
    public String userProfileUrl;
    public boolean profileStatus;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRecentMessage(String recentMessage) {
        this.recentMessage = recentMessage;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public void setUserProfileUrl(String userProfileUrl) {
        this.userProfileUrl = userProfileUrl;
    }

    public void setProfileStatus(boolean profileStatus){

        this.profileStatus=profileStatus;
    }

    public String getUserName() {
        return userName;
    }

    public String getRecentMessage() {
        return recentMessage;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public String getUserProfileUrl() {
        return userProfileUrl;
    }

    public boolean getProfileStatus() {
        return profileStatus;
    }
}
