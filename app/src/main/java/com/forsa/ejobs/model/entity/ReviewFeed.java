package com.forsa.ejobs.model.entity;

public class ReviewFeed {

    public String reviewerName;
    public String reviewerDescription;
    public float rating;
    public String ratingTime;

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public void setReviewerDescription(String reviewerDescription) {
        this.reviewerDescription = reviewerDescription;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setRatingTime(String ratingTime) {
        this.ratingTime = ratingTime;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public String getReviewerDescription() {
        return reviewerDescription;
    }

    public float getRating() {
        return rating;
    }

    public String getRatingTime() {
        return ratingTime;
    }
}
