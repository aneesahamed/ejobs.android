package com.forsa.ejobs.model.entity;

public class FavouriteFeed {

    public String rating;
    public String ratingCount;
    public String description;
    public String price;
    public String imageUrl;

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setRatingCount(String ratingCount) {
        this.ratingCount = ratingCount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getRatingCount() {
        return ratingCount;
    }


}
