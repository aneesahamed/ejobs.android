package com.forsa.ejobs.model.entity;

public class DeviceInfo {

    private String deviceId;
    private String deviceName;
    private String appVersion;
    private String osVersion;
    private String osType;

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String getOsType() {
        return osType;
    }
}
