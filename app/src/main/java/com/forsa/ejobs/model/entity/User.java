package com.forsa.ejobs.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {
    @SerializedName("success")
    int success;
    @SerializedName("userId")
    String userId;
    @SerializedName("name")
    String name;
    @SerializedName("email")
    String email;
    @SerializedName("mobile")
    String mobile;
    @SerializedName("profilePic")
    String profilePic;
    @SerializedName("cityId")
    String cityId;
    @SerializedName("cityName")
    String cityName;
    @SerializedName("userType")
    String userType;
    @SerializedName("authKey")
    String authKey;
    @SerializedName("error")
    int error;

    List<String> profilePictures;

    public void setProfilePictures(List<String> profilePictures) {
        this.profilePictures = profilePictures;
    }

    public List<String> getProfilePictures() {
        return profilePictures;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setError(int error) {
        this.error = error;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setProfilePic(String profilePic) {

    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public int getSuccess() {
        return success;
    }

    public int getError() {
        return error;
    }

    public String getAuthKey() {
        return authKey;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getProfilePic() {
        return profilePic;
    }


    public String getUserId() {
        return userId;
    }

    public String getUserType() {
        return userType;
    }

    public  boolean isProvider() {
        return getUserType().equals("P");
    }

}
