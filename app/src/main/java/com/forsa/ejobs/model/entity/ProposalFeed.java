package com.forsa.ejobs.model.entity;

public class ProposalFeed {

    String proposalImageUrl;
    String ProposalName;
    String ProposalPrice;

    public void setProposalImageUrl(String proposalImageUrl) {
        this.proposalImageUrl = proposalImageUrl;
    }

    public void setProposalName(String proposalName) {
        ProposalName = proposalName;
    }

    public void setProposalPrice(String proposalPrice) {
        ProposalPrice = proposalPrice;
    }

    public String getProposalImageUrl() {
        return proposalImageUrl;
    }


    public String getProposalName() {
        return ProposalName;
    }

    public String getProposalPrice() {
        return ProposalPrice;
    }
}
