package com.forsa.ejobs.model.entity;

import org.parceler.Parcel;

@Parcel
public class ServiceFeed {


    String serviceImageUrl;
    String serviceName;
    String serviceRating;
    String serviceCount;
    String serviceDescription;
    String servicePrice;
    boolean isFavouriteService;
    boolean isRecentService;

    public void setServiceImageUrl(String serviceImageUrl){

        this.serviceImageUrl=serviceImageUrl;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setServiceRating(String serviceRating) {
        this.serviceRating = serviceRating;
    }

    public void setServiceCount(String serviceCount) {
        this.serviceCount = serviceCount;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public void setIsFavouriteService(boolean favouriteService) {
        isFavouriteService = favouriteService;
    }

    public String getServiceImageUrl() {
        return serviceImageUrl;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getServiceRating() {
        return serviceRating;
    }

    public String getServiceCount() {
        return serviceCount;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public boolean getIsFavouriteService(){

        return isFavouriteService;
    }

    public void setRecentService(boolean recentService) {
        isRecentService = recentService;
    }

    public boolean getIsRecentService(){

        return isRecentService;
    }
}
