package com.forsa.ejobs.common;

import android.util.Log;

public class ErrorHandler {
    public static void handleError(int errorCode, String errorMessage) {
        Log.e("errorCode", String.valueOf(errorCode));
        Log.e("errorMsg", errorMessage);
    }
}
