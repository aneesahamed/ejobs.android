package com.forsa.ejobs.common;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.forsa.ejobs.model.entity.ProfileFeed;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FireBaseFileManager {
    static String filePath = AppConstant.EMPTY_STRING;
    private static int uploadCount = 0;

    public interface FileUploadListener {

        void onSuccess(String url);

        void onProgress(int progress);

        void onFailure(String message);
    }

    public interface RecursiveUpload {
        void onResponse(List<String> urls, List<String> failedPath);
    }

    public interface FireBasePath {
        String USER_PROFILE_PATH = "user/profilepic/";
    }

    public static void uploadPicture(String filePath, FileUploadListener listener) {
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        StorageReference storageReference = mStorageRef.child(FireBasePath.USER_PROFILE_PATH);
        uploadTask(storageReference, filePath, listener);
    }

    public static void uploadPictures(List<ProfileFeed> filePaths, RecursiveUpload upload) {
        List<String> resultUrl = new ArrayList<>();
        List<String> failedPath = new ArrayList<>();
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        StorageReference storageReference = mStorageRef.child(FireBasePath.USER_PROFILE_PATH);
        filePath = AppConstant.EMPTY_STRING;
        uploadCount = -1;
        for (int i = 0; i < filePaths.size(); i++) {
            filePath = filePaths.get(i).getFilePath();
            uploadTask(storageReference, filePaths.get(i).getFilePath(), new FileUploadListener() {
                @Override
                public void onSuccess(String url) {
                    resultUrl.add(url);
                    uploadCount++;
                    if(uploadCount==filePaths.size() -1){
                        upload.onResponse(resultUrl ,failedPath);

                    }

                }

                @Override
                public void onProgress(int progress) {

                }

                @Override
                public void onFailure(String message) {
                    failedPath.add(filePath);
                    uploadCount++;
                    if(uploadCount==filePaths.size() -1){
                        upload.onResponse(resultUrl ,failedPath);
                    }
                }
            });
        }

    }

    private static void uploadTask(final StorageReference storageReference, String filePath, FileUploadListener listener) {
        UploadTask uploadTask = storageReference.putFile(Uri.fromFile(new File(filePath)));
        uploadTask.addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            if (listener != null) {
                listener.onProgress((int) progress);
            }
        });

        uploadTask.continueWithTask(task -> {
            if (!task.isSuccessful()) {
                if (listener != null) {
                    listener.onFailure(task.getException().getMessage());
                }
            }
            // Continue with the task to get the download URL
            return storageReference.getDownloadUrl();
        }).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Uri downloadUri = task.getResult();
                if (listener != null) {
                    listener.onSuccess(downloadUri.toString());
                }
            } else {
                if (listener != null) {
                    listener.onFailure(task.getException().getMessage());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (listener != null) {
                    listener.onFailure(e.getMessage());
                }
            }
        });
    }
}

