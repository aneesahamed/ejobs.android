package com.forsa.ejobs.common;

import com.forsa.ejobs.ui.fragments.HomeFragment;
import com.forsa.ejobs.ui.fragments.NotificationFragment;
import com.forsa.ejobs.ui.message.MessageFragment;
import com.forsa.ejobs.ui.myOrder.MyOrderFragment;

public final class AppConstant {



    public static final String EMPTY_STRING="";
    public static final String DEVICE_SOURCE = "android";
    public static final String CATEGORY_LIST_FRAG ="categoryListFrag";
    public static final String CATEGORY_SUB_LIST_FRAG ="subCategoryListFrag";
    public static final String SERVICE_LIST_FRAG = "serviceListFrag";
    public static final String MY_REQUEST_FRAG="myRequestFrag";
    public static final String PROPOSAL_FRAG="proposalFrag";
    public static final String TYPE_GALLERY="gallery";
    public static final String TYPE_CAMERA="camera";
    public static final String TYPE_DOCUMENT="document";



    public interface BottomBarFragment{

        String HOME_FRAGMENT= HomeFragment.class.getSimpleName();
        String MESSAGE_FRAGMENT= MessageFragment.class.getSimpleName();
        String NOTIFICATION_FRAGMENT= NotificationFragment.class.getSimpleName();
        String MY_ORDER_FRAGMENT= MyOrderFragment.class.getSimpleName();
    }
}
