package com.forsa.ejobs.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.CityListAdapter;
import com.forsa.ejobs.model.entity.CityFeed;

import java.util.ArrayList;

public class EJobsDialogs extends DialogFragment {

    View mView;
    boolean isBottomDialog;
    public interface DialogListener{

      void optionClick(boolean isGallery);
    }


    public  static EJobsDialogs openGalleryChooser(final Context context){

        final EJobsDialogs eJobsDialogs = new EJobsDialogs();
        eJobsDialogs.mView = LayoutInflater.from(context).inflate(R.layout.layout_open_gallery_dialog, null, false);

        TextView OpenCamera = eJobsDialogs.mView.findViewById(R.id.actionOpenCamera);
        TextView OpenGallery = eJobsDialogs.mView.findViewById(R.id.actionOpenGallery);

        OpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogListener listener = (DialogListener)context;
                listener.optionClick(false);
            }
        });

        OpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogListener listener = (DialogListener)context;
                listener.optionClick(true);

            }
        });

        return eJobsDialogs;
    }
    public static EJobsDialogs openFilterDialog(Context context){

        final EJobsDialogs eJobsDialogs = new EJobsDialogs();
        eJobsDialogs.mView = LayoutInflater.from(context).inflate(R.layout.home_service_filter_dialog, null, false);

        EditText priceEditText = eJobsDialogs.mView.findViewById(R.id.priceValue);
        RatingBar ratingBar = eJobsDialogs.mView.findViewById(R.id.ratingBar);
        ImageView closeButton = eJobsDialogs.mView.findViewById(R.id.closeDialogButton);
        RecyclerView cityListRecycler = eJobsDialogs.mView.findViewById(R.id.cityList);
        cityListRecycler.setLayoutManager(new LinearLayoutManager(context));

        ArrayList<CityFeed> cList = new ArrayList<>();

        for(int i=0;i<4;i++){
            CityFeed cityFeed = new CityFeed();
            cityFeed.setCityName("chennai");
            cityFeed.setSelected(true);
            cList.add(cityFeed);
        }
        CityListAdapter cityListAdapter = new CityListAdapter(cList);
        cityListRecycler.setAdapter(cityListAdapter);

        Button clearAllButton = eJobsDialogs.mView.findViewById(R.id.clearAllButton);

        clearAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eJobsDialogs.dismiss();
            }
        });

         return eJobsDialogs;
    }

    public static EJobsDialogs openAgreedAmountDialog(Context context , boolean isBottomDialog){

        final EJobsDialogs eJobsDialogs = new EJobsDialogs();
        eJobsDialogs.isBottomDialog = isBottomDialog;
        eJobsDialogs.mView = LayoutInflater.from(context).inflate(R.layout.dialog_agreed_amount, null, false);

        //EditText priceEditText = eJobsDialogs.mView.findViewById(R.id.amountValue);
        //Button actionPay = eJobsDialogs.mView.findViewById(R.id.actionPay);

        return eJobsDialogs;

    }

/**
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.layout_open_gallery_dialog,container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        TextView OpenCamera = view.findViewById(R.id.actionOpenCamera);
        TextView OpenGallery = view.findViewById(R.id.actionOpenGallery);

        OpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogListener listener = (DialogListener)getActivity();
                listener.optionClick(false);
            }
        });

        OpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogListener listener = (DialogListener)getActivity();
                listener.optionClick(true);

            }
        });

        return view;
    }**/

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Context context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if(mView!=null){
            builder.setView(mView);
        }

        Dialog dialog = builder.create();
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(isBottomDialog){

            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }

        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        }



    }
