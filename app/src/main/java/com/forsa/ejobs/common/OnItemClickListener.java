package com.forsa.ejobs.common;

public interface OnItemClickListener {

    void onItemClick(Object object);

}
