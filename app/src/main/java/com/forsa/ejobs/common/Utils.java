package com.forsa.ejobs.common;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.forsa.ejobs.BuildConfig;
import com.forsa.ejobs.R;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.DeviceInfo;
import com.forsa.ejobs.model.entity.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.forsa.ejobs.common.AppConstant.DEVICE_SOURCE;

public class Utils {


    public interface AlertCallback{
        void onAccept();
        void onReject();
    }
    public static void setToolbarTitle(Toolbar toolbar, String toolbarTitle) {
        ((TextView) toolbar.findViewById(R.id.toolbarTitle)).setText(toolbarTitle);
    }

    public static boolean validateEmail(final String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void hideKeyBoard(Context context) {

    }

    public static String getFcmToken(Context context) {
        String fcmToken = PreferenceManager.getString(context, PreferenceManager.PrefKey.FCM_TOKEN, AppConstant.EMPTY_STRING);
        if (!TextUtils.isEmpty(fcmToken)) {
            return fcmToken;
        } else {
            return FirebaseInstanceId.getInstance().getToken();
        }
    }

    public void showAlertDialog(Context context ,String title ,String message ,boolean isFinish ,AlertCallback alertCallback){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(message);
        if(!TextUtils.isEmpty(title)){
            alertDialog.setTitle(title);
        }
        alertDialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(alertCallback != null){
                    alertCallback.onAccept();
                }
            }
        });
    }

    public static long getNumOfDay(long millis) {
        long msDiff = millis - Calendar.getInstance().getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(msDiff);
    }

    public static User getUser(Context context){
        String user = PreferenceManager.getString(context, PreferenceManager.PrefKey.USER, null);
        return new Gson().fromJson(user, User.class);
    }
    public static DeviceInfo getDeviceInfo(Context context) {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceId(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        deviceInfo.setDeviceName(android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);
        deviceInfo.setOsVersion(Build.VERSION.RELEASE);
        deviceInfo.setAppVersion(BuildConfig.VERSION_NAME);
        deviceInfo.setOsType(DEVICE_SOURCE);
        return deviceInfo;
    }

}
