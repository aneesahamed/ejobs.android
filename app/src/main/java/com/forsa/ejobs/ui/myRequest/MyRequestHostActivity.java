package com.forsa.ejobs.ui.myRequest;

import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.model.entity.ProposalFeed;
import com.forsa.ejobs.model.entity.RequestFeed;
import com.forsa.ejobs.ui.postRequest.PostRequestActivity;

import butterknife.ButterKnife;

public class MyRequestHostActivity extends AppCompatActivity implements FragmentListener {


    public static final String TAG_REQUEST_LIST=RequestListFragment.class.getSimpleName();
    public static final String TAG_REQUEST_DETAIL=MyRequestFragment.class.getSimpleName();
    public static final String TAG_PROPOSAL_DETAIL=ProposalDetailsFragment.class.getSimpleName();

    public boolean isMyRequestFrag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_REQUEST_LIST);
                if(fragment!=null && fragment.isVisible()){

                    isMyRequestFrag = true;
                    invalidateOptionsMenu();
                }
                else {

                    isMyRequestFrag = false;
                    invalidateOptionsMenu();
                }
            }
        });

        setUpInitialFragment();
    }

    public void callNextFragment(Object object ,String nextFragment){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        switch (nextFragment){

            case AppConstant.MY_REQUEST_FRAG:
                RequestFeed requestFeed = (RequestFeed)object;
                MyRequestFragment myRequestFragment = new MyRequestFragment();

                transaction.replace(R.id.myRequestFragContainer, myRequestFragment,TAG_REQUEST_DETAIL);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case AppConstant.PROPOSAL_FRAG:

                ProposalFeed proposalFeed = (ProposalFeed)object;

                ProposalDetailsFragment proposalDetailsFragment = new ProposalDetailsFragment();

                transaction.replace(R.id.myRequestFragContainer ,proposalDetailsFragment ,TAG_PROPOSAL_DETAIL);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
        }
    }

    public void setUpInitialFragment(){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        RequestListFragment requestListFragment = new RequestListFragment();
        transaction.add(R.id.myRequestFragContainer ,requestListFragment,TAG_REQUEST_LIST);
        transaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_items, menu);

        MenuItem postMenuItem = menu.findItem(R.id.action_postRequest);
        LinearLayout view = (LinearLayout)postMenuItem.getActionView();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyRequestHostActivity.this, PostRequestActivity.class);
                startActivity(intent);
            }
        });
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem menuPostCreate = menu.findItem(R.id.action_postRequest);

        if(isMyRequestFrag){

            menuPostCreate.setVisible(true);
        }
        else {
            menuPostCreate.setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);


    }

    @Override
    public void onBackPressed(){

        super.onBackPressed();
    }
}
