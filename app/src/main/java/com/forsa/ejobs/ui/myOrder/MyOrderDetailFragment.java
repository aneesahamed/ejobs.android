package com.forsa.ejobs.ui.myOrder;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.TimeLineAdapter;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.model.entity.TimeLineFeed;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MyOrderDetailFragment extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.myOrderItemImage)
    PorterShapeImageView myOrderImage;
    @BindView(R.id.myOrderTitle)
    TextView myOrderTitle;
    @BindView(R.id.myOrderDescription)
    TextView myOrderDescription;
    @BindView(R.id.orderPrice)
    TextView orderPrice;
    @BindView(R.id.orderStatus)
    TextView orderStatus;
    @BindView(R.id.timeLineRecyclerView)
    RecyclerView timeLineRecyclerView;
    @BindView(R.id.orderDetailButton)
    Button orderDetailButton;
    TimeLineAdapter timeLineAdapter;
    ArrayList<TimeLineFeed> timeLineFeeds = new ArrayList<>();


    public MyOrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_order_detail, container, false);
        unbinder = ButterKnife.bind(this , view);

        timeLineRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        timeLineRecyclerView.setNestedScrollingEnabled(false);
        timeLineAdapter = new TimeLineAdapter(getContext() ,timeLineFeeds);
        timeLineRecyclerView.setAdapter(timeLineAdapter);


        Glide.with(getActivity()).load("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg")
                .placeholder(R.drawable.place_holder_image)
                .error(R.drawable.place_holder_image)
                .into(myOrderImage);

        orderDetailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RatingBottomSheet ratingBottomSheet = new RatingBottomSheet();
                ratingBottomSheet.show(getFragmentManager(),RatingBottomSheet.class.getSimpleName());
            }
        });

        return  view;
    }



    @Override
    public void onResume(){
        super.onResume();

        getTimeLineData();
    }

    public void getTimeLineData(){

        ArrayList<TimeLineFeed> tList = new ArrayList<>();
        for(int i=0;i<5;i++){

            TimeLineFeed timeLineFeed = new TimeLineFeed();
            timeLineFeed.setTimeLineDescription(AppConstant.EMPTY_STRING);
            timeLineFeed.setTimeLineTitle("Lorem Ipsum");
            timeLineFeed.setItemTime("6:30pm");
            tList.add(timeLineFeed);
        }
        TimeLineFeed tl = new TimeLineFeed();
        tl.setTimeLineTitle("Lorem Ipsum");
        tl.setTimeLineDescription("Lorem ipsum lorem ipsum sum lorem ipsum sum lorem ipsum");
        tl.setItemTime("7:00pm");
        tl.setItemRating("5");
        tList.add(tl);

        timeLineFeeds.addAll(tList);
        timeLineAdapter.notifyDataSetChanged();

    }
    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }


}
