package com.forsa.ejobs.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.model.retrofit.RetrofitManager;
import com.google.gson.Gson;

public class LoginPresenter {
    private LoginInteractor interactor;

    public LoginPresenter(LoginInteractor interactor) {
        this.interactor = interactor;
    }

    public  void loginSignUp(Context context, String email, String password){
        if (isValid(email, password)){
            if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
                interactor.showProgressDialog();
                RetrofitManager.getInstance(context).userLogin(email, password, Utils.getFcmToken(context), new RetrofitManager.ResponseListener() {
                    @Override
                    public void onSuccess(Object object) {
                        User user = (User) object;
                        if (user != null) {
                            if (interactor != null) {
                                interactor.hideProgressDialog();
                                if (user.getSuccess() == 1) {
                                    String userResponse = new Gson().toJson(user);
                                    PreferenceManager.setString(context, PreferenceManager.PrefKey.USER, userResponse);
                                    interactor.launchHome();
                                }
                                else {
                                    if (user.getError() == 1) {
                                        interactor.showResponseError(1);
                                    } else if (user.getError() == 2) {
                                        interactor.showResponseError(2);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Object object){
                        if(interactor != null){
                            interactor.hideProgressDialog();
                        }
                    }

                });
            }
        }
    }

    private boolean isValid(String email, String password) {
        boolean isValid = true;
        if (TextUtils.isEmpty(password)) {
            isValid = false;
            interactor.showFieldError(0);

        } else if (TextUtils.isEmpty(email)) {
            isValid = false;
            interactor.showFieldError(1);
        } else if (!Utils.validateEmail(email)) {
            isValid = false;
            interactor.showFieldError(2);
        }
        return isValid;
    }
}

