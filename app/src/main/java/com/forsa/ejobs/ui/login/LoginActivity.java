package com.forsa.ejobs.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.ui.BaseActivity;
import com.forsa.ejobs.ui.home.HomeActivity;
import com.forsa.ejobs.ui.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginInteractor {

    @BindView(R.id.inputUsername)
    EditText inputUserName;
    @BindView(R.id.inputPassword)
    EditText inputPassword;
    @BindView(R.id.textForgotPassword)
    TextView textForgotPassword;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.textSignUp)
    TextView textSignUp;
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Utils.getUser(this) == null) {
            ButterKnife.bind(this);
            loginPresenter = new LoginPresenter(this);
            Spannable text = new SpannableString(getString(R.string.newUser));
            text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 10, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSignUp.setText(text);

            textSignUp.setOnClickListener(v -> {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            });

            loginButton.setOnClickListener(v -> {
                loginPresenter.loginSignUp(this, inputUserName.getText().toString(), inputPassword.getText().toString());
            });
        } else {
            launchHome();
        }
    }

    @Override
    public void showProgressDialog() {
        showLoader(getString(R.string.progress_text));
    }

    @Override
    public void hideProgressDialog() {
        hideLoader();
    }

    @Override
    public void launchHome() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void showFieldError(int errorCode) {
        switch (errorCode) {
            case 0:
                inputPassword.setError(getString(R.string.required));
                break;
            case 1:
                inputUserName.setError(getString(R.string.required));
                break;
            case 2:
                inputUserName.setError(getString(R.string.invalidEmail));
                break;
        }
    }

    @Override
    public void showResponseError(int errorCode) {
        if (errorCode == 1) {
            Toast.makeText(LoginActivity.this, getString(R.string.login_error1), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.login_error2), Toast.LENGTH_SHORT).show();
        }
    }
}
