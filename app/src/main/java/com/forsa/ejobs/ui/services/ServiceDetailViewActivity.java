package com.forsa.ejobs.ui.services;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.FragmentListener;

import butterknife.ButterKnife;

public class ServiceDetailViewActivity extends AppCompatActivity implements FragmentListener{


    public static final String TAG_SERVICE_DETAIL=ServiceDetailFragment.class.getSimpleName();
    public static final String TAG_REVIEW_LIST=ReviewListFragment.class.getSimpleName();
    public boolean isServiceDetailMenu =true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail_view);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_REVIEW_LIST);
                if(fragment!=null && fragment.isVisible()){

                    isServiceDetailMenu = false;
                    invalidateOptionsMenu();
                }
                else {

                    isServiceDetailMenu = true;
                    invalidateOptionsMenu();
                }
            }
        });
        setUpInitialFragment();




    }

    public void setUpInitialFragment(){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.serviceFragContainer , new ServiceDetailFragment() ,TAG_SERVICE_DETAIL);
        transaction.commit();

    }

    public void callNextFragment(Object object ,String nextFragment){

        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.serviceFragContainer ,new ReviewListFragment() ,TAG_REVIEW_LIST);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    @Override
    public void onResume(){

    super.onResume();
    invalidateOptionsMenu();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem menuShareItem = menu.findItem(R.id.action_share);
        MenuItem menuFavouriteItem = menu.findItem(R.id.action_favourite);

       if(isServiceDetailMenu){

           menuShareItem.setVisible(true);
           menuFavouriteItem.setVisible(true);
       }
       else {

           menuShareItem.setVisible(false);
           menuFavouriteItem.setVisible(false);
       }

        return super.onPrepareOptionsMenu(menu);


    }

    @Override
    public void onBackPressed(){

        super.onBackPressed();
    }
}
