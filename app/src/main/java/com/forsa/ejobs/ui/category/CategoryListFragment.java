package com.forsa.ejobs.ui.category;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.CategoryRecyclerViewAdapter;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.CategoryFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class CategoryListFragment extends Fragment {

    @BindView(R.id.categoryRecyclerView)
    RecyclerView categoryRecyclerView;
    CategoryRecyclerViewAdapter categoryRecyclerViewAdapter;

    private FragmentListener fragmentListener;
    Unbinder unbinder;
    ArrayList<CategoryFeed> categoryFeeds = new ArrayList<>();
    Toolbar toolbar;
    public CategoryListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this , view);

        toolbar = getActivity().findViewById(R.id.toolbar);

        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(categoryFeeds, false, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {


                if(fragmentListener!=null){

                    fragmentListener.callNextFragment(object , AppConstant.CATEGORY_SUB_LIST_FRAG);
                }

            }
        });
        categoryRecyclerView.setAdapter(categoryRecyclerViewAdapter);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        setToolbarTitle();
        getCategories();
    }

    public void setToolbarTitle(){

        ((TextView)toolbar.findViewById(R.id.toolbarTitle)).setText(R.string.categories);
    }


    public void getCategories(){

        for(int i=0;i<10;i++){

            CategoryFeed categoryFeed = new CategoryFeed();
            categoryFeed.setCategoryTitle("Lorem");
            categoryFeed.setCategoryDescription("Lorem Ipsum");

            categoryFeeds.add(categoryFeed);

        }

        categoryRecyclerViewAdapter.notifyDataSetChanged();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;

        } else {
            throw new RuntimeException(context.toString() + " must implement fragListener");
        }
    }

    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }


}
