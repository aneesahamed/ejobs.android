package com.forsa.ejobs.ui.myRequest;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.EJobsDialogs;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProposalDetailsFragment extends Fragment {

    private FragmentListener fragmentListener;
    Unbinder unbinder;
    @BindView(R.id.profileImage)
    CircleImageView proposalImage;
    @BindView(R.id.acceptButton)
    Button acceptButton;
    Toolbar toolbar;
    public ProposalDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_proposal_details, container, false);
        unbinder = ButterKnife.bind(this,view);

        toolbar = getActivity().findViewById(R.id.toolbar);

        Glide.with(getActivity()).load("https://cdn.vox-cdn.com/thumbor/NqIScowA_JqOvKanw3hVLOk2kpU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/53696927/apple-macbook-event-20161027-7271.0.0.jpg")
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(proposalImage);


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EJobsDialogs.openAgreedAmountDialog(getActivity(),true).show(getFragmentManager(),EJobsDialogs.class.getSimpleName());
            }
        });
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        Utils.setToolbarTitle(toolbar , getString(R.string.proposalDetails));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }
    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();

    }


}
