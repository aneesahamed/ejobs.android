package com.forsa.ejobs.ui.myRequest;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ProposalListRecyclerViewAdapter;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.entity.ProposalFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class MyRequestFragment extends Fragment {

    private FragmentListener fragmentListener;
    Unbinder unbinder;
    @BindView(R.id.profileImage)
    CircleImageView proposalImage;
    @BindView(R.id.title)
    TextView proposalTitle;
    @BindView(R.id.time)
    TextView proposalTime;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.proposalRecycler)
    RecyclerView proposalRecyclerView;
    ProposalListRecyclerViewAdapter proposalAdapter;
    ArrayList<ProposalFeed> proposalFeedList = new ArrayList<>();
    Toolbar toolbar;
    public MyRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_request, container, false);
        unbinder = ButterKnife.bind(this,view);

        toolbar = getActivity().findViewById(R.id.toolbar);
        proposalRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        proposalRecyclerView.setNestedScrollingEnabled(false);
        proposalAdapter = new ProposalListRecyclerViewAdapter(getActivity(), proposalFeedList, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                if(fragmentListener!=null){

                    fragmentListener.callNextFragment(object , AppConstant.PROPOSAL_FRAG);
                }
            }
        });

        proposalRecyclerView.setAdapter(proposalAdapter);

        Glide.with(getActivity()).load("https://cdn.vox-cdn.com/thumbor/NqIScowA_JqOvKanw3hVLOk2kpU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/53696927/apple-macbook-event-20161027-7271.0.0.jpg")
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(proposalImage);

        return view;
    }

    @Override
    public void onResume(){

        super.onResume();
        getProposalList();
        Utils.setToolbarTitle(toolbar ,getString(R.string.myRequest));
    }


    public void getProposalList(){

        ArrayList<ProposalFeed> pList = new ArrayList<>();

        for(int i=0;i<7;i++){

            ProposalFeed proposalFeed = new ProposalFeed();
            proposalFeed.setProposalImageUrl("https://cdn.vox-cdn.com/thumbor/NqIScowA_JqOvKanw3hVLOk2kpU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/53696927/apple-macbook-event-20161027-7271.0.0.jpg");
            proposalFeed.setProposalName("Lorem");
            proposalFeed.setProposalPrice("200");
            pList.add(proposalFeed);
        }

        proposalFeedList.addAll(pList);
        proposalAdapter.notifyDataSetChanged();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener= null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        unbinder.unbind();
    }

}
