package com.forsa.ejobs.ui.postRequest;

import androidx.appcompat.R;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.entity.PostRequestFeed;
import com.forsa.ejobs.ui.BaseActivity;
import com.forsa.ejobs.ui.home.HomeActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

public class PostRequestActivity extends BaseActivity implements PostRequestInteractor {

    @BindView(R.id.descText)
    EditText descText;
    @BindView(R.id.chooseCategorySpinner)
    Spinner categorySpinner;
    @BindView(R.id.categorySpinnerLayout)
    RelativeLayout categorySpinnerLayout;
    @BindView(R.id.subCategorySpinner)
    Spinner subCategorySpinner;
    @BindView(R.id.subCategorySpinnerLayout)
    RelativeLayout subCategorySpinnerLayout;
    @BindView(R.id.deliveryTimeSpinner)
    TextView deliveryTimeSpinner;
    @BindView(R.id.deliveryTimeSpinnerLayout)
    RelativeLayout deliveryTimeSpinnerLayout;
    @BindView(R.id.budgetValue)
    EditText budget;
    @BindView(R.id.txtCategory)
    TextView txtCategory;
    @BindView(R.id.txtSubCategory)
    TextView txtSubCategory;
    PostRequestPresenter postRequestPresenter;
    private int selectedCategoryPos;
    List<PostRequestFeed.Category> categoryList = new ArrayList<>();
    List<PostRequestFeed.Category> subCategoryList = new ArrayList<>();
    String categoryId;
    Calendar calendar;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post_request);
        ButterKnife.bind(this);
        postRequestPresenter = new PostRequestPresenter(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Utils.setToolbarTitle(toolbar, getString(R.string.post_a_request));
        toolbar.setNavigationOnClickListener(v ->  onBackPressed());
        subCategorySpinnerLayout.setOnClickListener(v -> subCategorySpinner.performClick());
        categorySpinnerLayout.setOnClickListener(v -> categorySpinner.performClick());
        deliveryTimeSpinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                String dayDifference = "";
                datePickerDialog = new DatePickerDialog(PostRequestActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int mYear,
                                                  int monthOfYear, int dayOfMonth) {
                                String selectedDate = dayOfMonth + "/" + monthOfYear + "/" + mYear;
                                String currentDate = day + "/" + month + "/" + year;
                                Date date1;
                                Date date2;
                                long difference = 0;
                                long differenceDates = 0;

                                try{
                                    SimpleDateFormat dates = new SimpleDateFormat("dd/MM/yyyy");
                                    date1 = dates.parse(currentDate);
                                    date2 = dates.parse(selectedDate);
                                    difference = Utils.getNumOfDay(date2.getTime());

                                }
                                catch(Exception ex){

                                }
                                String dayDifference = Long.toString(difference);
                                deliveryTimeSpinner.setText(dayDifference);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }


        });
        postRequestPresenter.getCategories(this);
    }

    @Override
    public void populateCategories(PostRequestFeed postRequestFeed) {
        categoryList.clear();
        categoryList.addAll(postRequestFeed.getCategoryList());
        categoryId = "";
        List<String> mCategoryList = new ArrayList<>();
        for (PostRequestFeed.Category category : postRequestFeed.getCategoryList()) {
            mCategoryList.add(category.getCategoryName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mCategoryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categoryList.size() > 0) {
                    txtCategory.setText(categoryList.get(position).getCategoryName());
                    categoryId = categoryList.get(position).getCategoryId();
                    postRequestPresenter.getCategories(getApplicationContext(), categoryId, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        categorySpinner.setSelection(0);

    }

    @Override
    public void populateSubCategories(PostRequestFeed postRequestFeed) {
        subCategoryList.clear();
        subCategoryList.addAll(postRequestFeed.getCategoryList());
        List<String> mSubCategoryList = new ArrayList<>();
        for (PostRequestFeed.Category subCategory : postRequestFeed.getCategoryList()) {
            mSubCategoryList.add(subCategory.getCategoryName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mSubCategoryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (subCategoryList.size() > 0) {
                    selectedCategoryPos = position;
                    txtSubCategory.setText(subCategoryList.get(position).getCategoryName());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subCategorySpinner.setAdapter(adapter);
        subCategorySpinner.setSelection(0);
    }


    @Override
    public void showProgressDialog() {
        showLoader(getString(R.string.progress_text));
    }

    @Override
    public void hideProgressDialog() {
        hideLoader();
    }

    @Override
    public void showFieldError(int errorCode) {

    }

    @Override
    public void showResponseError(int errorCode) {

    }
}
