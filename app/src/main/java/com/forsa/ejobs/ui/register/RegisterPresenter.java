package com.forsa.ejobs.ui.register;

import android.content.Context;
import android.text.TextUtils;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.ErrorHandler;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.model.retrofit.ErrorResponse;
import com.forsa.ejobs.model.retrofit.RetrofitManager;
import com.google.gson.Gson;

public class RegisterPresenter {
    private RegisterInteractor interactor;

    public RegisterPresenter(RegisterInteractor interactor) {
        this.interactor = interactor;
    }

    public void getCities(Context context) {
        if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
            RetrofitManager.getInstance(context).getCities(new RetrofitManager.ResponseListener() {
                @Override
                public void onSuccess(Object object) {
                    CityFeed cityFeed = (CityFeed) object;
                    if (cityFeed != null && cityFeed.getSuccess() == 1) {
                        if (interactor != null) {
                            interactor.populateCities(cityFeed);
                        }
                    }
                }

                @Override
                public void onFailure(Object object) {
                    ErrorResponse errorResponse = (ErrorResponse) object;
                    ErrorHandler.handleError(errorResponse.getErrorCode(), errorResponse.getErrorMessage());
                }
            });
        }
    }

    public void userSignUp(Context context, String userName, String email, String phoneNo, String password, String confirmPassword, String cityId, String uType) {
        if (isValid(userName, email, phoneNo, password, confirmPassword)) {
            if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
                interactor.showProgressDialog();
                RetrofitManager.getInstance(context).userSignUp(userName, email, phoneNo, password, Utils.getFcmToken(context), cityId, uType, new RetrofitManager.ResponseListener() {
                    @Override
                    public void onSuccess(Object object) {
                        User user = (User) object;
                        if (user != null) {
                            if (interactor != null) {
                                interactor.hideProgressDialog();
                                if (user.getSuccess() == 1) {
                                    String userResponse = new Gson().toJson(user);
                                    PreferenceManager.setString(context, PreferenceManager.PrefKey.USER, userResponse);
                                    interactor.launchHome();
                                }
                             else {
                                    if (user.getError() == 1) {
                                        interactor.showResponseError(1);
                                    } else if (user.getError() == 2) {
                                        interactor.showResponseError(2);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Object object) {
                        if(interactor != null)
                            interactor.hideProgressDialog();
                    }
                });
            }
        }
    }

    private boolean isValid(String userName, String email, String phoneNo, String password, String confirmPassword) {
        boolean isValid = true;
        if (TextUtils.isEmpty(userName)) {
            isValid = false;
            interactor.showFieldError(0);

        } else if (TextUtils.isEmpty(email)) {
            isValid = false;
            interactor.showFieldError(1);
        } else if (!Utils.validateEmail(email)) {
            isValid = false;
            interactor.showFieldError(2);

        } else if (TextUtils.isEmpty(phoneNo)) {
            isValid = false;
            interactor.showFieldError(3);

        } else if (TextUtils.isEmpty(password)) {
            isValid = false;
            interactor.showFieldError(4);

        } else if (TextUtils.isEmpty(confirmPassword)) {
            isValid = false;
            interactor.showFieldError(5);

        } else if (phoneNo.equals(confirmPassword)) {
            isValid = false;
            interactor.showFieldError(6);

        }

        return isValid;
    }
}
