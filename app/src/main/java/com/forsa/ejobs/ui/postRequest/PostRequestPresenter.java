package com.forsa.ejobs.ui.postRequest;

import android.content.Context;
import android.text.TextUtils;

import com.forsa.ejobs.common.ErrorHandler;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.CategoryFeed;
import com.forsa.ejobs.model.entity.PostRequestFeed;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.model.retrofit.ErrorResponse;
import com.forsa.ejobs.model.retrofit.RetrofitManager;
import com.google.gson.Gson;

public class PostRequestPresenter {
    private PostRequestInteractor interactor;

    private User user;

    public PostRequestPresenter(PostRequestInteractor interactor) {
        this.interactor = interactor;
    }

    public void getCategories(Context context) {
        getCategories(context, "0", false);
    }

    public void getCategories(Context context, String id, boolean subCategories) {
        if (user == null) {
            user = Utils.getUser(context);
        }
        if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
            RetrofitManager.getInstance(context).getCategories(user.getAuthKey(), id, new RetrofitManager.ResponseListener() {
                @Override
                public void onSuccess(Object object) {
                    PostRequestFeed categoryFeed = (PostRequestFeed) object;
                    if (categoryFeed != null && categoryFeed.getSuccess() == 1) {
                        if (interactor != null) {
                            if(!subCategories)
                                interactor.populateCategories(categoryFeed);
                            else
                                interactor.populateSubCategories(categoryFeed);
                        }
                    }
                }

                @Override
                public void onFailure(Object object) {
                    ErrorResponse errorResponse = (ErrorResponse) object;
                    ErrorHandler.handleError(errorResponse.getErrorCode(), errorResponse.getErrorMessage());
                }
            });
        }
    }

    public void PostRequest(Context context, String desc, int category, int delivery, int budget, String authKey) {
        if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
            interactor.showProgressDialog();
            RetrofitManager.getInstance(context).postRequest(authKey, desc, category, delivery, budget, new RetrofitManager.ResponseListener() {
                @Override
                public void onSuccess(Object object) {


                }

                @Override
                public void onFailure(Object object) {
                    if (interactor != null) {
                        interactor.hideProgressDialog();
                    }
                }

            });
        }
    }

    private boolean isValid(String desc, int category) {
        boolean isValid = true;
        if (TextUtils.isEmpty(desc)) {
            isValid = false;
            interactor.showFieldError(0);

        } else if (category == 0) {
            isValid = false;
            interactor.showFieldError(1);
        }
        return isValid;
    }
}
