package com.forsa.ejobs.ui.profile;

import android.content.Context;
import android.text.TextUtils;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.ErrorHandler;
import com.forsa.ejobs.common.FireBaseFileManager;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.BaseResponse;
import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.model.entity.ProfileFeed;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.model.retrofit.ErrorResponse;
import com.forsa.ejobs.model.retrofit.RetrofitManager;
import com.google.gson.Gson;

import java.util.List;

public class ProfilePresenter {
    private ProfileInteractor profileInteractor;

    public ProfilePresenter(ProfileInteractor profileInteractor) {
        this.profileInteractor = profileInteractor;
    }

    public void getCities(Context context) {
        if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
            RetrofitManager.getInstance(context).getCities(new RetrofitManager.ResponseListener() {
                @Override
                public void onSuccess(Object object) {
                    CityFeed cityFeed = (CityFeed) object;
                    if (cityFeed != null && cityFeed.getSuccess() == 1) {
                        if (profileInteractor != null) {
                            profileInteractor.populateCities(cityFeed);
                        }
                    }
                }

                @Override
                public void onFailure(Object object) {
                    ErrorResponse errorResponse = (ErrorResponse) object;
                    ErrorHandler.handleError(errorResponse.getErrorCode(), errorResponse.getErrorMessage());
                }
            });
        }
    }

    public void uploadProfile(Context context, boolean isProvider, String filePath, String name, String cityId, String phoneNo, List<ProfileFeed> filePathList) {
        if (isValid(name, phoneNo)) {
            if (RetrofitManager.getInstance(context).isConnectingToInternet()) {
                profileInteractor.showProgressDialog();
                User user = Utils.getUser(context);
                if(!isProvider) {
                    if (!TextUtils.isEmpty(filePath)) {

                        FireBaseFileManager.uploadPicture(filePath, new FireBaseFileManager.FileUploadListener() {
                            @Override
                            public void onSuccess(String url) {
                                uploadProfileData(context, name, cityId, phoneNo, url, user.getAuthKey());
                                profileInteractor.hideProgressDialog();
                            }

                            @Override
                            public void onProgress(int progress) {

                            }

                            @Override
                            public void onFailure(String message) {
                                if (profileInteractor != null) {
                                    profileInteractor.hideProgressDialog();
                                    profileInteractor.showNetworkError();
                                }
                            }
                        });
                    } else {


                        uploadProfileData(context , name, cityId, phoneNo, user.getProfilePic(), user.getAuthKey());
                    }
                }
                else{
                    FireBaseFileManager.uploadPictures(filePathList, new FireBaseFileManager.RecursiveUpload() {
                        @Override
                        public void onResponse(List<String> urls, List<String> failedPath) {
                            if(urls.size()>0){
                                uploadProfileData(context  ,name,cityId ,phoneNo,TextUtils.join(",",urls),user.getAuthKey());
                            }
                        }
                    });
                }
            }
        }
    }

    private void uploadProfileData(Context context, String name, String cityId, String phoneNo, String profileUrl, String authKey){
        RetrofitManager.getInstance(context).profileUpload(authKey, name, cityId, phoneNo, profileUrl, new RetrofitManager.ResponseListener() {
                @Override
                public void onSuccess(Object object) {
                    BaseResponse response = (BaseResponse)object;
                    profileInteractor.hideProgressDialog();
                    if(response.success() == 1){
                        User user = Utils.getUser(context);
                        user.setProfilePic(profileUrl);
                        PreferenceManager.setString(context , PreferenceManager.PrefKey.USER,new Gson().toJson(user));
                        profileInteractor.onResponse(context.getString(R.string.profile_upload_msg));
                    }
                    else
                    {
                        profileInteractor.onResponse(context.getString(R.string.register_error2));
                    }
                }

                @Override
                public void onFailure(Object object) {
                    if(profileInteractor != null){
                        profileInteractor.hideProgressDialog();
                    }
                }
        });
    }

    private boolean isValid(String name, String phoneNo) {
        boolean isValid = true;
        if (TextUtils.isEmpty(name)) {
            isValid = false;
            profileInteractor.showFieldError(0);

        }  else if (TextUtils.isEmpty(phoneNo)) {
            isValid = false;
            profileInteractor.showFieldError(3);

        }

        return isValid;
    }
}
