package com.forsa.ejobs.ui.register;

import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.ui.BaseInteractor;

import java.util.List;

public interface RegisterInteractor extends BaseInteractor {
    @Override
    void hideProgressDialog();

    @Override
    void showProgressDialog();

    void showFieldError(int message);

    void populateCities(CityFeed cityFeed);

    void launchHome();

    void showResponseError(int error);
}
