package com.forsa.ejobs.ui.message;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatThreadActivity extends AppCompatActivity implements MediaAttachmentDialog.ClickListener{

    @BindView(R.id.chatAttachment)
    RelativeLayout chatAttachment;
    @BindView(R.id.chatToolbar)
    Toolbar toolbar;
    MediaAttachmentDialog mediaAttachmentDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_thread);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageView backNav = toolbar.findViewById(R.id.backNavigation);
        CircleImageView profilePic = toolbar.findViewById(R.id.chatProfile);
        ((TextView)toolbar.findViewById(R.id.chatUserName)).setText("Lorem");

        backNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        Glide.with(this).load("http://img2.thejournal.ie/inline/2470754/original?width=428&version=2470754")
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(profilePic);
        chatAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaAttachmentDialog = new MediaAttachmentDialog();
                mediaAttachmentDialog.show(getSupportFragmentManager(),MediaAttachmentDialog.class.getSimpleName());

            }
        });
    }

    public void onClick(String selection){

        mediaAttachmentDialog.dismiss();

        switch (selection){

            case AppConstant.TYPE_CAMERA:
                String s="";
                break;
            case AppConstant.TYPE_GALLERY:
                break;
            case AppConstant.TYPE_DOCUMENT:
                break;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem optionItems=menu.findItem(R.id.action_Options);
        optionItems.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_square) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
