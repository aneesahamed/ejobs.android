package com.forsa.ejobs.ui.login;

import com.forsa.ejobs.ui.BaseInteractor;

public interface LoginInteractor extends BaseInteractor {

    @Override
    void hideProgressDialog();

    @Override
    void showProgressDialog();

    void showFieldError(int message);

    void launchHome();

    void showResponseError(int errorCode);
}
