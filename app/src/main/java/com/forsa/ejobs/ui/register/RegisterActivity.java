package com.forsa.ejobs.ui.register;

import android.content.Intent;
import android.graphics.PorterDuff;

import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.forsa.ejobs.R;
import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.ui.BaseActivity;
import com.forsa.ejobs.ui.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity implements RegisterInteractor , AdapterView.OnItemSelectedListener{

    @BindView(R.id.toHirePartLayout)
    RelativeLayout toHirePartLayout;
    @BindView(R.id.toHirePartImage)
    ImageView toHirePartImage;
    @BindView(R.id.toHireText)
    TextView toHireText;
    @BindView(R.id.toWorkPartLayout)
    RelativeLayout toWorkPartLayout;
    @BindView(R.id.toWorkPartImage)
    ImageView toWorkPartImage;
    @BindView(R.id.toWorkText)
    TextView toWorkText;
    @BindView(R.id.inputUsername)
    EditText inputUserName;
    @BindView(R.id.inputEmail)
    EditText inputEmail;
    @BindView(R.id.inputPhoneNo)
    EditText inputPhoneNo;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    @BindView(R.id.spinnerLayout)
    RelativeLayout spinnerLayout;
    @BindView(R.id.citySpinnerTitle)
    TextView citySpinnerTitle;
    @BindView(R.id.txtCity)
    TextView txtCity;
    @BindView(R.id.inputPassword)
    EditText inputPassword;
    @BindView(R.id.inputConfirmPassword)
    EditText inputConfirmPassword;
    @BindView(R.id.registerButton)
    Button registerButton;
    @BindView(R.id.textSignIn)
    TextView textSignIn;
    RegisterPresenter registerPresenter;
    private int selectedCityPos;
    List<CityFeed.City> cityList = new ArrayList<>();
    private boolean isProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setUpInitialView();
        registerPresenter = new RegisterPresenter(this);
        Spannable text = new SpannableString(getString(R.string.allReadyHaveAnAccount));
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 25, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSignIn.setText(text);
        textSignIn.setOnClickListener(v -> {
            finish();
        });
        spinnerLayout.setOnClickListener(v -> {
            citySpinner.performClick();
        });
        registerPresenter.getCities(this);
        registerButton.setOnClickListener(v -> {
            if(cityList.size() >0){
                CityFeed.City city = cityList.get(selectedCityPos);
                String cityId = city.getId();
                registerPresenter.userSignUp(this, inputUserName.getText().toString(), inputEmail.getText().toString(), inputPhoneNo.getText().toString(), inputPassword.getText().toString(), inputConfirmPassword.getText().toString(), cityId, isProvider ? "P": "U" );
            }

        });
    }

    public void setUpInitialView() {

        isProvider = false;
        resetSwitchLayout();
        toHirePartLayout.setBackgroundResource(R.drawable.bg_hire_work);
        toHirePartImage.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        toHireText.setTextColor(getResources().getColor(R.color.colorPrimary));

    }

    public void viewSwitch(View view) {

        resetSwitchLayout();
        switch (view.getId()) {

            case R.id.toHirePartLayout:

                isProvider = false;
                toHirePartLayout.setBackgroundResource(R.drawable.bg_hire_work);
                toHirePartImage.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
                toHireText.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;

            case R.id.toWorkPartLayout:

                isProvider = true;
                toWorkPartLayout.setBackgroundResource(R.drawable.bg_hire_work);
                toWorkPartImage.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
                toWorkText.setTextColor(getResources().getColor(R.color.colorPrimary));

                break;
        }
    }


    public void resetSwitchLayout() {

        toHirePartLayout.setBackgroundResource(R.drawable.bg_hire_default);
        toWorkPartLayout.setBackgroundResource(R.drawable.bg_work_default);
        toHirePartImage.setColorFilter(ContextCompat.getColor(this, R.color.mediumDarkGrey), PorterDuff.Mode.SRC_IN);
        toWorkPartImage.setColorFilter(ContextCompat.getColor(this, R.color.mediumDarkGrey), PorterDuff.Mode.SRC_IN);
        toHireText.setTextColor(getResources().getColor(R.color.mediumDarkGrey));
        toWorkText.setTextColor(getResources().getColor(R.color.mediumDarkGrey));
    }

    @Override
    public void showProgressDialog() {
        showLoader(getString(R.string.progress_text));
    }

    @Override
    public void hideProgressDialog() {
        hideLoader();
    }

    @Override
    public void launchHome() {
        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void populateCities(CityFeed cityFeed) {
        cityList.clear();
        cityList.addAll(cityFeed.getCityList());
        List<String> mCityList = new ArrayList<>();
        for (CityFeed.City city : cityFeed.getCityList()){
            mCityList.add(city.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mCityList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setOnItemSelectedListener(this);
        citySpinner.setAdapter(adapter);
        citySpinner.setSelection(0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(cityList.size()>0){
            selectedCityPos = position;
            txtCity.setText(cityList.get(position).getName());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void showResponseError(int errorCode)
    {
        if(errorCode == 1){
            Toast.makeText(RegisterActivity.this, getString(R.string.register_error1), Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(RegisterActivity.this, getString(R.string.register_error2), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showFieldError(int errorCode) {
        switch (errorCode){
            case 0:
                inputUserName.setError(getString(R.string.required));
                break;
            case 1:
                inputEmail.setError(getString(R.string.required));
                break;
            case 2:
                inputEmail.setError(getString(R.string.invalidEmail));
                break;
            case 3:
                inputPhoneNo.setError(getString(R.string.required));
                break;
            case 4:
                inputPassword.setError(getString(R.string.required));
                break;
            case 5:
                inputConfirmPassword.setError(getString(R.string.required));
                break;
            case 6:
                inputConfirmPassword.setError(getString(R.string.password_not_matching));
                break;
        }
    }
}
