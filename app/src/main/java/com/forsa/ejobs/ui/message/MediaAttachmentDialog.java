package com.forsa.ejobs.ui.message;


import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.AppConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class MediaAttachmentDialog extends BottomSheetDialogFragment {


     public interface ClickListener{
        void onClick(String selection);
    }

    public MediaAttachmentDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_media_attachment_dialog, container, false);

        LinearLayout camera = rootView.findViewById(R.id.actionCamera);
        LinearLayout gallery = rootView.findViewById(R.id.actionGallery);
        LinearLayout document = rootView.findViewById(R.id.actionDocument);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClickListener clickListener = (ClickListener)getActivity();
                clickListener.onClick(AppConstant.TYPE_CAMERA);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClickListener clickListener = (ClickListener)getActivity();
                clickListener.onClick(AppConstant.TYPE_GALLERY);
            }
        });

        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClickListener clickListener = (ClickListener)getActivity();
                clickListener.onClick(AppConstant.TYPE_DOCUMENT);
            }
        });


        return rootView;
    }

}
