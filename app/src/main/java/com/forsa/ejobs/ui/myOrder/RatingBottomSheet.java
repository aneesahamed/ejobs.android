package com.forsa.ejobs.ui.myOrder;


import android.graphics.PorterDuff;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.forsa.ejobs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingBottomSheet extends BottomSheetDialogFragment {


    public RatingBottomSheet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.rating_bottomsheet, container, false);

        ImageView actionClose = rootView.findViewById(R.id.closeBottomSheet);
        TextView ratingDescription = rootView.findViewById(R.id.ratingText);
        RatingBar ratingBar = rootView.findViewById(R.id.ratingBar);
        EditText reviewEdit = rootView.findViewById(R.id.reviewEditText);
        reviewEdit.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        Button buttonSubmit = rootView.findViewById(R.id.buttonSubmit);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        actionClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });
        return rootView;
    }

}
