package com.forsa.ejobs.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ServiceHorizontalRecyclerViewAdapter;
import com.forsa.ejobs.adapter.SliderAdapter;
import com.forsa.ejobs.adapter.TopCategoriesRecyclerViewAdapter;
import com.forsa.ejobs.ui.category.CategoryHostActivity;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.CategoryFeed;
import com.forsa.ejobs.model.entity.HomeFeed;
import com.forsa.ejobs.model.entity.ServiceFeed;
import com.forsa.ejobs.ui.myRequest.MyRequestHostActivity;
import com.github.demono.AutoScrollViewPager;
import com.rd.PageIndicatorView;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class HomeFragment extends Fragment {

    @BindView(R.id.homeSlider)
    AutoScrollViewPager loopingViewPager;
    @BindView(R.id.indicator)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.homeCategoryRecyclerView)
    RecyclerView homeCategoryRecyclerView;
    @BindView(R.id.horizontalPromotedItemRecycler)
    RecyclerView promotedItemRecyclerView;
    @BindView(R.id.recentlyViewedRecycler)
    RecyclerView recentlyViewedRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.categoryViewAll)
    TextView categoryViewAll;
    @BindView(R.id.postRequestButton)
    Button postRequestButton;

    TopCategoriesRecyclerViewAdapter topCategoriesRecyclerViewAdapter;
    ServiceHorizontalRecyclerViewAdapter promotedItemRecyclerViewAdapter;
    ServiceHorizontalRecyclerViewAdapter recentItemRecyclerViewAdapter;
    SliderAdapter sliderAdapter;
    Unbinder unbinder;
    ArrayList<HomeFeed.SliderFeed> sliderFeedList=new ArrayList<>();
    ArrayList<CategoryFeed> categoryFeedList=new ArrayList<>();
    ArrayList<ServiceFeed> serviceFeedList=new ArrayList<>();
    ArrayList<ServiceFeed> recentServiceList = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_home, container, false);
        unbinder= ButterKnife.bind(this,view);

        sliderAdapter=new SliderAdapter(getActivity(),createDummyData().getSliderFeedList(),false,true);
        loopingViewPager.setAdapter(sliderAdapter);
        loopingViewPager.startAutoScroll();
        pageIndicatorView.setCount(createDummyData().getSliderFeedList().size());
        pageIndicatorView.setViewPager(loopingViewPager);

        loopingViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                pageIndicatorView.setSelection(position);

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




        homeCategoryRecyclerView.setNestedScrollingEnabled(false);
        homeCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        topCategoriesRecyclerViewAdapter=new TopCategoriesRecyclerViewAdapter(categoryFeedList, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                CategoryFeed categoryFeed = (CategoryFeed)object;
                Intent intent = new Intent(getActivity() ,CategoryHostActivity.class);
                intent.putExtra("isFromViewAll",false);
                intent.putExtra("categoryFeed", Parcels.wrap(categoryFeed));
                startActivity(intent);

            }
        });
        homeCategoryRecyclerView.setAdapter(topCategoriesRecyclerViewAdapter);

        promotedItemRecyclerView.setNestedScrollingEnabled(false);
        promotedItemRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        promotedItemRecyclerViewAdapter=new ServiceHorizontalRecyclerViewAdapter(getActivity(),serviceFeedList);
        promotedItemRecyclerView.setAdapter(promotedItemRecyclerViewAdapter);

        recentlyViewedRecyclerView.setNestedScrollingEnabled(false);
        recentlyViewedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        recentItemRecyclerViewAdapter=new ServiceHorizontalRecyclerViewAdapter(getActivity(),recentServiceList);
        recentlyViewedRecyclerView.setAdapter(recentItemRecyclerViewAdapter);


        categoryViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), CategoryHostActivity.class);
                intent.putExtra("isFromViewAll",true);
                startActivity(intent);

            }
        });

        postRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity() , MyRequestHostActivity.class);
                startActivity(intent);
            }
        });

        return  view;
    }




    @Override
    public void onResume() {
        super.onResume();

        //loopingViewPager.resumeAutoScroll();
        progressBar.setVisibility(View.VISIBLE);
        populateData();

    }

    @Override
    public void onPause() {

        super.onPause();
        //loopingViewPager.pauseAutoScroll();

    }

    public void populateData(){

        HomeFeed homeFeed=createDummyData();
        progressBar.setVisibility(View.INVISIBLE);
        //sliderAdapter.setItemList(homeFeed.getSliderFeedList());
        //loopingViewPager.reset();

        categoryFeedList.clear();
        categoryFeedList.addAll(createDummyData().getCategoryFeedList());
        topCategoriesRecyclerViewAdapter.notifyDataSetChanged();

        serviceFeedList.clear();
        serviceFeedList.addAll(homeFeed.getPromotedItemFeedList());

        recentServiceList.addAll(homeFeed.getRecentlyItemFeedList());

        promotedItemRecyclerViewAdapter.notifyDataSetChanged();
        recentItemRecyclerViewAdapter.notifyDataSetChanged();


    }

    public HomeFeed createDummyData(){

        ArrayList<HomeFeed.SliderFeed> mList=new ArrayList<>();
        for(int i=0;i<7;i++){

            HomeFeed.SliderFeed sliderFeed=new HomeFeed.SliderFeed();
            sliderFeed.setSliderTitle("Categories");
            sliderFeed.setSliderDescription("Lorem Ipsum is simply dummy text of the printing ");
            sliderFeed.setSliderUrl("https://icdn4.digitaltrends.com/image/android_phone_feat-720x720.jpg?ver=1.jpg");
            sliderFeed.setIsForHomeSlider(true);
            mList.add(sliderFeed);
        }

        ArrayList<CategoryFeed> cList=new ArrayList<>();

        for (int i=0;i<8;i++){

            CategoryFeed categoryFeed=new CategoryFeed();
            categoryFeed.setCategoryTitle("Category");
            categoryFeed.setCategoryDescription("Lorem Ipsum is simply dummy text of the printing");
            cList.add(categoryFeed);
        }

        ArrayList<ServiceFeed> sList=new ArrayList<>();

        for(int i=0;i<8;i++){

            ServiceFeed serviceFeed=new ServiceFeed();
            serviceFeed.setServiceName("Lorem");
            serviceFeed.setServiceImageUrl("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg");
            serviceFeed.setServiceCount("5");
            serviceFeed.setServiceRating("4.5");
            serviceFeed.setIsFavouriteService(true);
            serviceFeed.setServicePrice("200");
            serviceFeed.setRecentService(false);
            sList.add(serviceFeed);
        }

        ArrayList<ServiceFeed> rList=new ArrayList<>();

        for(int i=0;i<8;i++){

            ServiceFeed serviceFeed=new ServiceFeed();
            serviceFeed.setServiceName("Lorem");
            serviceFeed.setServiceImageUrl("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg");
            serviceFeed.setServiceCount("5");
            serviceFeed.setServiceRating("4.5");
            serviceFeed.setIsFavouriteService(true);
            serviceFeed.setServicePrice("200");
            serviceFeed.setRecentService(true);
            rList.add(serviceFeed);
        }

        HomeFeed homeFeed=new HomeFeed();
        homeFeed.setSliderFeedList(mList);
        homeFeed.setCategoryFeedList(cList);
        homeFeed.setPromotedItemFeedList(sList);
        homeFeed.setRecentlyItemFeedList(rList);
        return homeFeed;

    }

    @Override
    public void onStop(){

        super.onStop();
    }

    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();

    }

}
