package com.forsa.ejobs.ui.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.FavouriteRecyclerViewAdapter;
import com.forsa.ejobs.model.entity.FavouriteFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FavoriteFragment extends Fragment {



    Unbinder unbinder;
    @BindView(R.id.favouriteRecyclerView)
    RecyclerView favouriteRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    FavouriteRecyclerViewAdapter adapter;
    ArrayList<FavouriteFeed> feedList=new ArrayList<>();
    public FavoriteFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_favorite, container, false);
        unbinder= ButterKnife.bind(this,view);

        adapter = new FavouriteRecyclerViewAdapter(getActivity() , feedList);
        favouriteRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        favouriteRecyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume(){

        super.onResume();
        getFeedList();

    }

    public void getFeedList() {

        ArrayList<FavouriteFeed> fList = new ArrayList<>();

        for(int i=0;i<10;i++){

            FavouriteFeed favouriteFeed = new FavouriteFeed();
            favouriteFeed.setImageUrl("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg");
            favouriteFeed.setRating("4");
            favouriteFeed.setRatingCount("15");
            favouriteFeed.setDescription("Lorem Ipsum");
            favouriteFeed.setPrice("200");
            fList.add(favouriteFeed);
        }

        feedList.addAll(fList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }



}
