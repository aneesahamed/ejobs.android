package com.forsa.ejobs.ui.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.NotificationListRecyclerViewAdapter;
import com.forsa.ejobs.model.entity.NotificationFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class NotificationFragment extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.NotificationListRecyclerView)
    RecyclerView notificationRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    NotificationListRecyclerViewAdapter notificationListAdapter;
    ArrayList<NotificationFeed> notificationFeeds=new ArrayList<>();
    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder= ButterKnife.bind(this,view);

        notificationListAdapter=new NotificationListRecyclerViewAdapter(getActivity(),notificationFeeds);
        notificationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        notificationRecyclerView.setAdapter(notificationListAdapter);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        getNotificationList();
        
    }

    public void getNotificationList() {

        notificationFeeds.addAll(createDummyData());
        progressBar.setVisibility(View.INVISIBLE);
        notificationListAdapter.notifyDataSetChanged();

    }
    
    public ArrayList<NotificationFeed> createDummyData(){

        ArrayList<NotificationFeed> cList=new ArrayList<>();

        for(int i=0;i<10;i++){

            NotificationFeed notificationFeed=new NotificationFeed();
            notificationFeed.setUserProfileUrl("https://icdn4.digitaltrends.com/image/android_phone_feat-720x720.jpg?ver=1.jpg");
            notificationFeed.setProfileStatus(true);
            notificationFeed.setReceivedTime("10mins ago");
            notificationFeed.setTitle("Lorem Ipsum");
            notificationFeed.setContent("Lorem Ipsum Message");
            cList.add(notificationFeed);

        }

        return cList;
    }
    
    @Override
    public void onDestroyView(){
        
        super.onDestroyView();
        unbinder.unbind();
    }


}
