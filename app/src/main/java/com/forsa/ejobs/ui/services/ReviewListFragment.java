package com.forsa.ejobs.ui.services;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ReviewListAdapter;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.model.entity.ReviewFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ReviewListFragment extends Fragment {

    private FragmentListener fragmentListener;
    Unbinder unbinder;
    Toolbar toolbar;
    @BindView(R.id.reviewRecyclerView)
    RecyclerView reviewRecyclerView;
    ReviewListAdapter reviewListAdapter;
    ArrayList<ReviewFeed> reviewFeedList = new ArrayList<>();

    public ReviewListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review_list, container, false);
        unbinder = ButterKnife.bind(this ,view);

        toolbar = getActivity().findViewById(R.id.toolbar);
        reviewRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        reviewListAdapter = new ReviewListAdapter(getActivity() , reviewFeedList);
        reviewRecyclerView.setAdapter(reviewListAdapter);
        return view;

    }

    @Override
    public void onResume(){

        super.onResume();
        populateData();
        setToolbarTitle();
    }

    public void populateData(){

        ArrayList<ReviewFeed> rList = new ArrayList<>();

        for(int i=0 ;i<10 ;i++){

            ReviewFeed reviewFeed = new ReviewFeed();
            reviewFeed.setRatingTime("6:30pm");
            reviewFeed.setReviewerName("Lorem");
            reviewFeed.setReviewerDescription("Lorem ipsum");
            reviewFeed.setRating(Float.valueOf("2.5"));
            rList.add(reviewFeed);
        }

        reviewFeedList.addAll(rList);
        reviewListAdapter.notifyDataSetChanged();
    }

    public void setToolbarTitle(){

        ((TextView)toolbar.findViewById(R.id.toolbarTitle)).setText(R.string.reviews);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener= null;
    }

    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }



}
