package com.forsa.ejobs.ui;

public interface BaseInteractor {
    void showProgressDialog();
    void hideProgressDialog();
}
