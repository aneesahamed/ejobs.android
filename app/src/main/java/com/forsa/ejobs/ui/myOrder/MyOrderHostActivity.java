package com.forsa.ejobs.ui.myOrder;

import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.forsa.ejobs.R;

import butterknife.ButterKnife;

public class MyOrderHostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(getString(R.string.orderDetails));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        setUpInitialFragment();

    }

    public void setUpInitialFragment(){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        MyOrderDetailFragment myOrderDetailFragment = new MyOrderDetailFragment();
        fragmentTransaction.add(R.id.myOrderFragContainer , myOrderDetailFragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed(){

        super.onBackPressed();
    }

}
