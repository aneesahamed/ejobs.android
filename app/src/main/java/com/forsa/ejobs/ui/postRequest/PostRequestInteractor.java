package com.forsa.ejobs.ui.postRequest;

import com.forsa.ejobs.model.entity.PostRequestFeed;
import com.forsa.ejobs.ui.BaseInteractor;

public interface PostRequestInteractor extends BaseInteractor {
    @Override
    void hideProgressDialog();

    @Override
    void showProgressDialog();

    void showFieldError(int message);

    void showResponseError(int errorCode);

    void populateCategories(PostRequestFeed postRequestFeed);

    void populateSubCategories(PostRequestFeed postRequestFeed);
}
