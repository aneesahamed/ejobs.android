package com.forsa.ejobs.ui.services;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ServiceHorizontalRecyclerViewAdapter;
import com.forsa.ejobs.adapter.SliderAdapter;
import com.forsa.ejobs.common.EJobsDialogs;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.model.entity.HomeFeed;
import com.forsa.ejobs.model.entity.ServiceFeed;
import com.github.demono.AutoScrollViewPager;
import com.rd.PageIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class ServiceDetailFragment extends Fragment {

    private FragmentListener fragmentListener;

    Unbinder unbinder;
    @BindView(R.id.buttonOrder)
    FrameLayout buttonOrder;
    @BindView(R.id.homeSlider)
    AutoScrollViewPager loopingViewPager;
    @BindView(R.id.indicator)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.recentlyViewedRecycler)
    RecyclerView recentlyViewedRecyclerView;
    @BindView(R.id.chatProfile)
    CircleImageView profilePicture;
    @BindView(R.id.reviewList)
    LinearLayout reviewList;
    ServiceHorizontalRecyclerViewAdapter serviceHorizontalRecyclerViewAdapter;
    ArrayList<ServiceFeed> serviceFeeds = new ArrayList<>();
    SliderAdapter sliderAdapter;
    public ServiceDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_service_detail, container, false);
        unbinder = ButterKnife.bind(this,view);


        pageIndicatorView.setViewPager(loopingViewPager);
        loopingViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                pageIndicatorView.setSelection(position);

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        recentlyViewedRecyclerView.setNestedScrollingEnabled(false);
        recentlyViewedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        serviceHorizontalRecyclerViewAdapter = new ServiceHorizontalRecyclerViewAdapter(getActivity() ,serviceFeeds);
        recentlyViewedRecyclerView.setAdapter(serviceHorizontalRecyclerViewAdapter);

        Glide.with(getActivity()).load("https://cdn.vox-cdn.com/thumbor/NqIScowA_JqOvKanw3hVLOk2kpU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/53696927/apple-macbook-event-20161027-7271.0.0.jpg")
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(profilePicture);


        reviewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentListener.callNextFragment(null ,null);
            }
        });

        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EJobsDialogs.openAgreedAmountDialog(getActivity(),false).show(getFragmentManager(),EJobsDialogs.class.getSimpleName());
            }
        });
        return view;
    }

    

    @Override
    public void onResume(){

        super.onResume();
        setToolbarTitle();
        populateData();
    }

    public void setToolbarTitle(){

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        ((TextView)toolbar.findViewById(R.id.toolbarTitle)).setText(getString(R.string.services));
    }

    public void populateData(){

        ArrayList<HomeFeed.SliderFeed> mList=new ArrayList<>();
        for(int i=0;i<7;i++){

            HomeFeed.SliderFeed sliderFeed=new HomeFeed.SliderFeed();
            sliderFeed.setSliderTitle("Categories");
            sliderFeed.setSliderDescription("Lorem Ipsum is simply dummy text of the printing ");
            sliderFeed.setSliderUrl("https://icdn4.digitaltrends.com/image/android_phone_feat-720x720.jpg?ver=1.jpg");
            sliderFeed.setIsForHomeSlider(false);
            mList.add(sliderFeed);
        }

        sliderAdapter=new SliderAdapter(getActivity(),mList,false,false);
        loopingViewPager.setAdapter(sliderAdapter);
        loopingViewPager.startAutoScroll();
        pageIndicatorView.setCount(mList.size());


        ArrayList<ServiceFeed> sList=new ArrayList<>();

        for(int i=0;i<8;i++){

            ServiceFeed serviceFeed=new ServiceFeed();
            serviceFeed.setServiceName("Lorem");
            serviceFeed.setServiceImageUrl("https://icdn4.digitaltrends.com/image/android_phone_feat-720x720.jpg?ver=1.jpg");
            serviceFeed.setServiceCount("5");
            serviceFeed.setServiceRating("4.5");
            serviceFeed.setIsFavouriteService(true);
            serviceFeed.setServicePrice("200");
            serviceFeed.setRecentService(true);
            sList.add(serviceFeed);
        }

        serviceFeeds.addAll(sList);
        serviceHorizontalRecyclerViewAdapter.notifyDataSetChanged();


    }
    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }


}
