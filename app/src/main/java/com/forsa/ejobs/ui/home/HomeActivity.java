package com.forsa.ejobs.ui.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.PreferenceManager;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.ui.fragments.ContactUsFragment;
import com.forsa.ejobs.ui.fragments.FavoriteFragment;
import com.forsa.ejobs.ui.fragments.HomeFragment;
import com.forsa.ejobs.ui.fragments.NotificationFragment;
import com.forsa.ejobs.ui.login.LoginActivity;
import com.forsa.ejobs.ui.message.MessageFragment;
import com.forsa.ejobs.ui.myOrder.MyOrderFragment;
import com.forsa.ejobs.ui.profile.ProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity  {



    @BindView(R.id.navProfile)
    CircleImageView navProfilePicture;
    @BindView(R.id.bottomNavigation)
    RelativeLayout bottomNavigationBar;
    @BindView(R.id.iconHome)
    ImageView iconHome;
    @BindView(R.id.iconMessage)
    ImageView iconMessage;
    @BindView(R.id.iconNotification)
    ImageView iconNotification;
    @BindView(R.id.myOrderIcon)
    ImageView iconMyOrder;
    @BindView(R.id.homeIndicator)
    View homeIndicator;
    @BindView(R.id.messageIndicator)
    View messageIndicator;
    @BindView(R.id.notificationIndicator)
    View notificationIndicator;
    @BindView(R.id.myOrderIndicator)
    View myOrderIndicator;
    @BindView(R.id.searchBox)
    RelativeLayout searchBarLayout;
    @BindView(R.id.searchEditText)
    EditText searchEditText;

    DrawerLayout drawer;
    int selectedBottomItem;
    int selectedNavItem;
    private boolean isHomeFragment=true;
    TextView toolbarTitle;
    ActionBarDrawerToggle toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User user = Utils.getUser(this);
        if(user == null)
        {
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbarTitle=(TextView)toolbar.findViewById(R.id.toolbarTitle);
        setSupportActionBar(toolbar);
        resetBottomBarItemColor();
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        Glide.with(this).load("https://cdn.vox-cdn.com/thumbor/NqIScowA_JqOvKanw3hVLOk2kpU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/53696927/apple-macbook-event-20161027-7271.0.0.jpg")
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(navProfilePicture);

        navProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawers();
                Intent intent = new Intent(HomeActivity.this , ProfileActivity.class);
                startActivity(intent);

            }
        });

        searchEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    hideKeyboard();
                    //check input value
                    return true;
                }
                return false;
            }
        });

        setUpInitialFragment();

    }


    public void setUpInitialFragment(){

        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer,new HomeFragment());
        fragmentTransaction.commit();

        bottomNavigationBar.setVisibility(View.VISIBLE);
        updateBottomBarIndicator(R.id.actionHome);
        updateNavItemsColor(R.id.navHome);

        selectedBottomItem =R.id.actionHome;
        selectedNavItem=R.id.navHome;
        isHomeFragment=true;


        toolbarTitle.setText(getString(R.string.home));
        searchBarLayout.setVisibility(View.VISIBLE);

        toggle.setDrawerIndicatorEnabled(true);
        toggle.setDrawerSlideAnimationEnabled(true);
        toggle.setToolbarNavigationClickListener(toggle.getToolbarNavigationClickListener());
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

    }

    public void setUpToolbarItems(int selectedItem){

        switch (selectedItem){

            case R.id.actionHome:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.home));
                searchBarLayout.setVisibility(View.VISIBLE);

                setUpBackNavigation(false);

                break;
            case R.id.actionMessage:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.messages));
                searchBarLayout.setVisibility(View.GONE);

                setUpBackNavigation(true);
                break;
            case R.id.actionNotification:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.notification));
                searchBarLayout.setVisibility(View.GONE);

                setUpBackNavigation(true);
                break;
            case R.id.actionMyOrder:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.myOrders));
                searchBarLayout.setVisibility(View.GONE);

                setUpBackNavigation(true);
                break;

            case R.id.navFavourite:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.favourite));
                searchBarLayout.setVisibility(View.GONE);

                setUpBackNavigation(false);
                break;

            case R.id.navContactUs:

                invalidateOptionsMenu();
                toolbarTitle.setText(getString(R.string.contactUs));
                searchBarLayout.setVisibility(View.GONE);

                setUpBackNavigation(false);
                break;

        }

    }

    public void setUpBackNavigation(boolean isHome){

        if(!isHome){

            toggle.setDrawerIndicatorEnabled(true);
            toggle.setDrawerSlideAnimationEnabled(true);
            toggle.setToolbarNavigationClickListener(toggle.getToolbarNavigationClickListener());
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
        else {

            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setUpInitialFragment();
                    invalidateOptionsMenu();
                }
            });
        }

    }

    public void navItemClick(View view){

        drawer.closeDrawers();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        switch (view.getId()){

            case R.id.navHome:

                if(!(selectedNavItem==view.getId())){

                    isHomeFragment=true;
                    transaction.replace(R.id.fragmentContainer,new HomeFragment());
                    selectedNavItem=view.getId();
                    selectedBottomItem=R.id.actionHome;
                    setUpToolbarItems(R.id.actionHome);
                    updateNavItemsColor(R.id.navHome);
                    bottomNavigationBar.setVisibility(View.VISIBLE);
                    updateBottomBarIndicator(R.id.actionHome);

                }
                break;
            case R.id.navMessages:

                if(!(selectedNavItem==view.getId())) {

                    isHomeFragment=false;
                    transaction.replace(R.id.fragmentContainer , new MessageFragment());
                    selectedNavItem=view.getId();
                    selectedBottomItem=R.id.actionMessage;
                    setUpToolbarItems(R.id.actionMessage);
                    updateNavItemsColor(R.id.navMessages);
                    bottomNavigationBar.setVisibility(View.VISIBLE);
                    updateBottomBarIndicator(R.id.actionMessage);

                }
                break;
            case R.id.navNotification:

                if(!(selectedNavItem==view.getId())){

                    isHomeFragment=false;
                    transaction.replace(R.id.fragmentContainer , new NotificationFragment());
                    selectedNavItem=view.getId();
                    selectedBottomItem=R.id.actionNotification;
                    setUpToolbarItems(R.id.actionNotification);
                    updateNavItemsColor(R.id.navNotification);
                    bottomNavigationBar.setVisibility(View.VISIBLE);
                    updateBottomBarIndicator(R.id.actionNotification);

                }
                break;
            case R.id.navPostRequest:

                break;
            case R.id.navMyRequest:

                break;
            case R.id.navMyOrder:

                if(!(selectedNavItem==view.getId())){

                    isHomeFragment=false;
                    transaction.replace(R.id.fragmentContainer , new MyOrderFragment());
                    selectedNavItem=view.getId();
                    selectedBottomItem=R.id.actionMyOrder;
                    setUpToolbarItems(R.id.actionMyOrder);
                    updateNavItemsColor(R.id.navMyOrder);
                    bottomNavigationBar.setVisibility(View.VISIBLE);
                    updateBottomBarIndicator(R.id.actionMyOrder);

                }

                break;
            case R.id.navFavourite:

                if(!(selectedNavItem==view.getId())){

                    isHomeFragment=false;
                    transaction.replace(R.id.fragmentContainer , new FavoriteFragment());
                    selectedNavItem=view.getId();
                    setUpToolbarItems(R.id.navFavourite);
                    updateNavItemsColor(R.id.navFavourite);
                    bottomNavigationBar.setVisibility(View.GONE);

                }
                break;
            case R.id.navSpecialRequest:

                break;
            case R.id.navContactUs:

                if(!(selectedNavItem==view.getId())){

                    isHomeFragment=false;
                    transaction.replace(R.id.fragmentContainer , new ContactUsFragment());
                    selectedNavItem=view.getId();
                    setUpToolbarItems(R.id.navContactUs);
                    updateNavItemsColor(R.id.navContactUs);
                    bottomNavigationBar.setVisibility(View.GONE);

                }

                break;
            case R.id.navLogout:
                PreferenceManager.clearPreferenceString(this, PreferenceManager.PrefKey.USER);
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }

        transaction.commit();

    }

    public void updateNavItemsColor(int selectedNavItem){

        resetNavItemsColor();

        switch (selectedNavItem){

            case R.id.navHome:

                ((TextView)findViewById(R.id.navHomeText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navHomeIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navHomeIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navMessages:

                ((TextView)findViewById(R.id.navMessageText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navMessageIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navMessageIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navNotification:

                ((TextView)findViewById(R.id.navNotificationText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navNotificationIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navNotificationIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navPostRequest:

                ((TextView)findViewById(R.id.navPostRequestText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navPostRequestIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navPostRequestIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navMyRequest:

                ((TextView)findViewById(R.id.navMyRequestText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navMyRequestIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navMyRequestIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navMyOrder:

                ((TextView)findViewById(R.id.navMyOrderText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navMyOrderIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navMyOrderIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navFavourite:

                ((TextView)findViewById(R.id.navFavouriteText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navFavouriteIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navFavouriteIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navSpecialRequest:

                ((TextView)findViewById(R.id.navSpecialRequestText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navSpecialRequestIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navSpecialRequestIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;
            case R.id.navContactUs:

                ((TextView)findViewById(R.id.navContactUsText)).setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                ((View)findViewById(R.id.navContactUsIndicator)).setVisibility(View.VISIBLE);
                ((View)findViewById(R.id.navContactUsIndicator)).setBackgroundResource(R.color.colorPrimary);

                break;

        }

    }

    public void resetNavItemsColor(){

        ((TextView)findViewById(R.id.navHomeText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navMessageText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navNotificationText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navPostRequestText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navMyOrderText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navMyRequestText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navFavouriteText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navSpecialRequestText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));
        ((TextView)findViewById(R.id.navContactUsText)).setTextColor(ContextCompat.getColor(this,R.color.mediumDarkGrey));

        ((View)findViewById(R.id.navHomeIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navMessageIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navNotificationIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navPostRequestIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navMyOrderIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navMyRequestIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navFavouriteIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navSpecialRequestIndicator)).setVisibility(View.INVISIBLE);
        ((View)findViewById(R.id.navContactUsIndicator)).setVisibility(View.INVISIBLE);

    }

    public void bottomNavigationClick(View view) {

        drawer.closeDrawers();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (view.getId()) {

            case R.id.actionHome:
                if(!(selectedBottomItem ==view.getId())){

                    isHomeFragment=true;
                    HomeFragment homeFragment =new HomeFragment();
                    fragmentTransaction.replace(R.id.fragmentContainer,homeFragment);
                    selectedBottomItem =view.getId();
                    updateBottomBarIndicator(view.getId());
                    fragmentTransaction.commit();
                    setUpToolbarItems(view.getId());
                    selectedNavItem = R.id.navHome;
                    updateNavItemsColor(R.id.navHome);

                }
                break;
            case R.id.actionMessage:

                if(!(selectedBottomItem ==view.getId())){

                    isHomeFragment=false;
                    MessageFragment messageFragment = new MessageFragment();
                    fragmentTransaction.replace(R.id.fragmentContainer,messageFragment);
                    selectedBottomItem =view.getId();
                    updateBottomBarIndicator(view.getId());
                    fragmentTransaction.commit();
                    setUpToolbarItems(view.getId());
                    /*searchBarLayout.animate().translationY(searchBarLayout.getHeight()).alpha(0.0f)
                            .setDuration(50)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    searchBarLayout.setVisibility(View.GONE);
                                }
                            });*/

                }
                break;
            case R.id.actionNotification:

                if(!(selectedBottomItem ==view.getId())){

                    isHomeFragment=false;
                    NotificationFragment notificationFragment=new NotificationFragment();
                    fragmentTransaction.replace(R.id.fragmentContainer,notificationFragment);
                    selectedBottomItem =view.getId();
                    updateBottomBarIndicator(view.getId());
                    fragmentTransaction.commit();
                    setUpToolbarItems(view.getId());

                }
                break;
            case R.id.actionMyOrder:

                if(!(selectedBottomItem ==view.getId())){

                    isHomeFragment=false;
                    MyOrderFragment myOrderFragment=new MyOrderFragment();
                    fragmentTransaction.replace(R.id.fragmentContainer,myOrderFragment);
                    selectedBottomItem =view.getId();
                    updateBottomBarIndicator(view.getId());
                    fragmentTransaction.commit();
                    setUpToolbarItems(view.getId());

                }
                break;


        }



    }

    private void resetBottomBarItemColor() {
        iconHome.setColorFilter(ContextCompat.getColor(this,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);
        iconMessage.setColorFilter(ContextCompat.getColor(this,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);
        iconNotification.setColorFilter(ContextCompat.getColor(this,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);
        iconMyOrder.setColorFilter(ContextCompat.getColor(this,R.color.mediumDarkGrey),android.graphics.PorterDuff.Mode.SRC_IN);

        homeIndicator.setVisibility(View.INVISIBLE);
        messageIndicator.setVisibility(View.INVISIBLE);
        notificationIndicator.setVisibility(View.INVISIBLE);
        myOrderIndicator.setVisibility(View.INVISIBLE);

    }

    private void updateBottomBarIndicator(int selectedItem){

        resetBottomBarItemColor();

        switch (selectedItem)
        {
            case R.id.actionHome:
                iconHome.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
                homeIndicator.setVisibility(View.VISIBLE);
                homeIndicator.setBackgroundResource(R.color.colorPrimary);
                break;

            case R.id.actionMessage:
                iconMessage.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
                messageIndicator.setVisibility(View.VISIBLE);
                messageIndicator.setBackgroundResource(R.color.colorPrimary);
                break;

            case R.id.actionNotification:
                iconNotification.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
                notificationIndicator.setVisibility(View.VISIBLE);
                notificationIndicator.setBackgroundResource(R.color.colorPrimary);
                break;

            case R.id.actionMyOrder:
                iconMyOrder.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary),android.graphics.PorterDuff.Mode.SRC_IN);
                myOrderIndicator.setVisibility(View.VISIBLE);
                myOrderIndicator.setBackgroundResource(R.color.colorPrimary);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if(!isHomeFragment) {

                setUpInitialFragment();
                invalidateOptionsMenu();
            }
            else {

                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem dashBoardItem=menu.findItem(R.id.action_square);

        if(isHomeFragment){

            dashBoardItem.setVisible(true);
        }
        else {
            dashBoardItem.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_square) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(){

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

}
