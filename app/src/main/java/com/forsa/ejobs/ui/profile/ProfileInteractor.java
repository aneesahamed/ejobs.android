package com.forsa.ejobs.ui.profile;

import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.ui.BaseInteractor;

import java.util.List;

public interface ProfileInteractor extends BaseInteractor {
    @Override
    void showProgressDialog();

    @Override
    void hideProgressDialog();

    void showFieldError(int errorCode);

    void populateCities(CityFeed cityFeed);

    void showNetworkError();

    void onProfileUploadResponse();
    void onResponse(String message);
}
