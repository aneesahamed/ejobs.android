package com.forsa.ejobs.ui.category;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.forsa.ejobs.R;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.EJobsDialogs;
import com.forsa.ejobs.model.entity.CategoryFeed;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryHostActivity extends AppCompatActivity implements FragmentListener {

    public static final String TAG_CATEGORIES = CategoryListFragment.class.getSimpleName();
    public static final String TAG_SUB_CATEGORY = SubCategoryListFragment.class.getSimpleName();
    public static final String TAG_SERVICE_LIST = ServiceListFragment.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    boolean isFromViewAll;
    boolean isSearchMenuVisible = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        isFromViewAll = getIntent().getBooleanExtra("isFromViewAll" , false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        if(isFromViewAll){

            setUpInitialFragment();
        }
        else {


            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            SubCategoryListFragment subCategoryListFragment = new SubCategoryListFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable("categoryFeed",getIntent().getParcelableExtra("categoryFeed"));
            subCategoryListFragment.setArguments(bundle);

            transaction.add(R.id.categoryFragContainer,subCategoryListFragment,TAG_SUB_CATEGORY);
            transaction.commit();

        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_SERVICE_LIST);
                if(fragment!=null && fragment.isVisible()){

                    isSearchMenuVisible = false;
                    invalidateOptionsMenu();
                }
                else {

                    isSearchMenuVisible = true;
                    invalidateOptionsMenu();
                }
            }
        });

    }

    public void setUpInitialFragment(){



        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.categoryFragContainer , new CategoryListFragment(),TAG_CATEGORIES);
        fragmentTransaction.commit();
    }

    public void callNextFragment(Object object , String nextFragment){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (nextFragment) {

            case AppConstant.CATEGORY_SUB_LIST_FRAG:



                CategoryFeed categoryFeed = (CategoryFeed)object;

                Bundle bundle = new Bundle();
                bundle.putParcelable("categoryFeed",Parcels.wrap(categoryFeed));

                SubCategoryListFragment subCategoryListFragment = new SubCategoryListFragment();
                subCategoryListFragment.setArguments(bundle);

                transaction.replace(R.id.categoryFragContainer , subCategoryListFragment ,TAG_SUB_CATEGORY);
                transaction.addToBackStack(null);


                break;
            case AppConstant.SERVICE_LIST_FRAG:

                CategoryFeed serviceFeed = (CategoryFeed)object;
                transaction.replace(R.id.categoryFragContainer , new ServiceListFragment() ,TAG_SERVICE_LIST);
                transaction.addToBackStack(null);


                break;
        }

        transaction.commit();

    }

    @Override
    public void onResume(){

        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem menuSearchItem = menu.findItem(R.id.actionSearch);
        MenuItem menuFilterItem = menu.findItem(R.id.action_filter);

        if(isSearchMenuVisible){

            menuFilterItem.setVisible(false);
            menuSearchItem.setVisible(true);
        }
        else {

            menuSearchItem.setVisible(false);
            menuFilterItem.setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionSearch) {

            return true;
        }
        else if(id==R.id.action_filter){

            EJobsDialogs.openFilterDialog(this).show(getSupportFragmentManager(),EJobsDialogs.class.getSimpleName());
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){


        super.onBackPressed();

    }
    }
