package com.forsa.ejobs.ui.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ServiceListRecyclerViewAdapter;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ServiceFeed;
import com.forsa.ejobs.ui.services.ServiceDetailViewActivity;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ServiceListFragment extends Fragment {



    @BindView(R.id.serviceListRecyclerView)
    RecyclerView serviceRecyclerView;
    ServiceListRecyclerViewAdapter adapter;
    Unbinder unbinder;
    private FragmentListener fragmentListener;
    ArrayList<ServiceFeed> serviceFeeds = new ArrayList<>();
    Toolbar toolbar;
    public ServiceListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_service_list, container, false);
        unbinder = ButterKnife.bind(this,view);
        toolbar = getActivity().findViewById(R.id.toolbar);


        adapter = new ServiceListRecyclerViewAdapter(getActivity(), serviceFeeds, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                if(fragmentListener !=null){

                    ServiceFeed serviceFeed = (ServiceFeed)object;

                    Intent intent = new Intent(getActivity() , ServiceDetailViewActivity.class);
                    intent.putExtra("serviceFeed",Parcels.wrap(serviceFeed));
                    startActivity(intent);


                }
            }
        });
        serviceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        serviceRecyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        setToolbarTitle();
        getServiceList();
    }

    public void setToolbarTitle(){

        ((TextView)toolbar.findViewById(R.id.toolbarTitle)).setText(R.string.services);
    }
    public void getServiceList(){


        for(int i=0;i<10;i++){

            ServiceFeed serviceFeed = new ServiceFeed();
            serviceFeed.setServiceDescription("Lorem Ipsum..");
            serviceFeed.setServiceRating("4.5");
            serviceFeed.setServiceCount("08");
            serviceFeed.setServiceImageUrl("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg");
            serviceFeed.setServicePrice("200");

            serviceFeeds.add(serviceFeed);
        }

        adapter.notifyDataSetChanged();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        unbinder.unbind();
    }

}
