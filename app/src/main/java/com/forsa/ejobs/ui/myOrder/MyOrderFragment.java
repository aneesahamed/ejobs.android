package com.forsa.ejobs.ui.myOrder;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.MyOrderListRecyclerViewAdapter;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.MyOrderFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MyOrderFragment extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.MyOrderListRecyclerView)
    RecyclerView myOrderRecyclerView;
    MyOrderListRecyclerViewAdapter adapter;
    ArrayList<MyOrderFeed> myOrderFeeds = new ArrayList<>();

    public MyOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.fragment_my_order, container, false);
        unbinder= ButterKnife.bind(this,view);

        myOrderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new MyOrderListRecyclerViewAdapter(getActivity(), myOrderFeeds, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                MyOrderFeed myOrderFeed = (MyOrderFeed)object;
                Intent intent = new Intent(getActivity() ,MyOrderHostActivity.class);
                startActivity(intent);

            }
        });
        myOrderRecyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume(){

        super.onResume();
        getData();
    }

    public void getData(){

        myOrderFeeds.addAll(createDummyData());
        adapter.notifyDataSetChanged();
    }

    public ArrayList<MyOrderFeed> createDummyData(){

        ArrayList<MyOrderFeed> mList = new ArrayList<>();

         for(int i=0;i<10;i++){

             MyOrderFeed myOrderFeed = new MyOrderFeed();
             myOrderFeed.setImageUrl("https://picjumbo.com/wp-content/uploads/creative-designer-photographer-workspace_free_stock_photos_picjumbo_P1000571-2210x1475.jpg");
             myOrderFeed.setMyOrderName("Lorem Ipsum");
             myOrderFeed.setMyOrderDescription("Lorem Ipsum printing and");
             myOrderFeed.setMyOrderTime("3:30pm");
             myOrderFeed.setMyOrderPrice("$20");

             mList.add(myOrderFeed);
         }

        return mList;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        unbinder.unbind();
    }






}
