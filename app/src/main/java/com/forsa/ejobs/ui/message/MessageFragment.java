package com.forsa.ejobs.ui.message;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ChatListRecyclerViewAdapter;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.model.entity.ChatListFeed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MessageFragment extends Fragment {


    @BindView(R.id.chatListRecyclerView)
    RecyclerView chatListRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    ChatListRecyclerViewAdapter chatListRecyclerViewAdapter;
    Unbinder unbinder;
    ArrayList<ChatListFeed> chatListFeeds=new ArrayList<>();
    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_message, container, false);
        unbinder= ButterKnife.bind(this,view);

        chatListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        chatListRecyclerViewAdapter=new ChatListRecyclerViewAdapter(getActivity(), chatListFeeds, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                Intent intent = new Intent(getActivity() , ChatThreadActivity.class);
                startActivity(intent);

            }
        });
        chatListRecyclerView.setAdapter(chatListRecyclerViewAdapter);


        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        getChatList();
    }

    public void getChatList(){

        chatListFeeds.addAll(createDummyData());
        progressBar.setVisibility(View.INVISIBLE);
        chatListRecyclerViewAdapter.notifyDataSetChanged();
    }

    public ArrayList<ChatListFeed> createDummyData(){

        ArrayList<ChatListFeed> cList=new ArrayList<>();

        for(int i=0;i<10;i++){

            ChatListFeed chatListFeed=new ChatListFeed();
            chatListFeed.setUserProfileUrl("https://icdn4.digitaltrends.com/image/android_phone_feat-720x720.jpg?ver=1.jpg");
            chatListFeed.setProfileStatus(true);
            long unix_seconds = 1372339860;
            Date date =new Date(unix_seconds*1000);
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("hh:mm a");
            chatListFeed.setReceivedTime(simpleDateFormat.format(date));
            chatListFeed.setUserName("Lorem Ipsum");
            chatListFeed.setRecentMessage("Lorem Ipsum Message");
            chatListFeed.setUnReadCount(3);
            cList.add(chatListFeed);

        }

        return cList;
    }

    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();

    }



}
