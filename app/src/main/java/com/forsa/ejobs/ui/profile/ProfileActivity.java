package com.forsa.ejobs.ui.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.ProfileGridAdapter;
import com.forsa.ejobs.adapter.ProfileSliderAdapter;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.EJobsDialogs;
import com.forsa.ejobs.common.FileHelper;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.entity.CityFeed;
import com.forsa.ejobs.model.entity.ProfileFeed;
import com.forsa.ejobs.model.entity.User;
import com.forsa.ejobs.ui.BaseActivity;
import com.github.demono.AutoScrollViewPager;
import com.rd.PageIndicatorView;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;


public class ProfileActivity extends BaseActivity implements EJobsDialogs.DialogListener ,ProfileInteractor, AdapterView.OnItemSelectedListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.actionEdit)
    RelativeLayout actionEdit;
    @BindView(R.id.profilePicture)
    CircleImageView profilePicture;
    @BindView(R.id.userProfileLayout)
    FrameLayout userProfileLayout;
    @BindView(R.id.sliderLayout)
    RelativeLayout sliderLayout;
    @BindView(R.id.profileSlider)
    AutoScrollViewPager slider;
    @BindView(R.id.indicator)
    PageIndicatorView pageIndicator;
    @BindView(R.id.profileGridRecycler)
    RecyclerView profileGridRecyclerView;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    @BindView(R.id.spinnerLayout)
    RelativeLayout spinnerLayout;
    @BindView(R.id.citySpinnerTitle)
    TextView citySpinnerTitle;
    @BindView(R.id.txtCity)
    TextView txtCity;
    @BindView(R.id.inputUsername)
    EditText edName;
    @BindView(R.id.inputPhoneNo)
    EditText edPhoneNo;
    ProfileGridAdapter profileGridAdapter;
    ProfileSliderAdapter profileSliderAdapter;
    TextView editSaveText, toolbarTitle;
    ImageView navBack;
    ArrayList<ProfileFeed> filePathList = new ArrayList<>();
    public static final String TAG_DIALOG = EJobsDialogs.class.getSimpleName();
    public boolean isInEditMode = true;
    static final int RESULT_OPEN_GALLERY = 0;
    static final int RESULT_OPEN_CAMERA = 1;
    File tempFile;
    String filePath;
    EJobsDialogs dialogFragment;
    User user;
    public boolean isProvider = true;
    ProfilePresenter presenter;
    private int selectedCityPos;
    List<CityFeed.City> cityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        presenter = new ProfilePresenter(this);
        editSaveText = toolbar.findViewById(R.id.toolbarAction);
        navBack = toolbar.findViewById(R.id.actionNavBack);
        user = Utils.getUser(this);
        toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        citySpinner.setSelection(Integer.parseInt(user.getCityId()));
        spinnerLayout.setOnClickListener(v -> {
            citySpinner.performClick();
        });
        presenter.getCities(this);
        ProfileFeed profileFeed = new ProfileFeed();
        profileFeed.setFilePath(AppConstant.EMPTY_STRING);
        profileFeed.setIsAddItem(true);
        filePathList.add(profileFeed);

        isProvider = user.isProvider();
        if (isProvider) {

            editSaveText.setVisibility(View.VISIBLE);
            slider.setVisibility(View.VISIBLE);
            pageIndicator.setVisibility(View.VISIBLE);
            profileGridRecyclerView.setVisibility(View.INVISIBLE);
            userProfileLayout.setVisibility(View.INVISIBLE);

            getPictureList();

        } else {

            editSaveText.setVisibility(View.VISIBLE);
            slider.setVisibility(View.INVISIBLE);
            pageIndicator.setVisibility(View.INVISIBLE);
            profileGridRecyclerView.setVisibility(View.INVISIBLE);
            userProfileLayout.setVisibility(View.VISIBLE);

            setUserProfile(user.getProfilePic());
        }

        navBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        editSaveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInEditMode) {
                    isInEditMode = false;
                    editSaveText.setText(getString(R.string.save));

                    if (isProvider) {

                        switchToEditMode();
                        initiateGridRecycler();
                    } else {

                        actionEdit.setVisibility(View.VISIBLE);
                    }


                } else {

                    isInEditMode = true;
                    editSaveText.setText(getString(R.string.edit));

                    if (isProvider) {

                        switchToSaveMode();
                        populateSlider();

                        ArrayList<ProfileFeed> finalList = profileGridAdapter.getFilePathList();
                        finalList.remove(finalList.size()-1);
                        if(finalList.size()>0){
                            uploadProfilePic(isProvider, finalList);
                        }
                    } else {

                        actionEdit.setVisibility(View.GONE);
                        uploadProfilePic(isProvider, null);
                    }


                }

            }
        });


        actionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog();
            }
        });

    }

    public void getPictureList() {

        populateSlider();

    }

    public void populateSlider() {

        ArrayList<ProfileFeed> sliderList = new ArrayList<>(filePathList);
        if (filePathList.size() > 1) {

            sliderList.remove(sliderList.size() - 1);
        }

        profileSliderAdapter = new ProfileSliderAdapter(this, sliderList, false);
        slider.setAdapter(profileSliderAdapter);
        slider.startAutoScroll();
        pageIndicator.setCount(sliderList.size());
        pageIndicator.setViewPager(slider);

    }

    public void initiateGridRecycler() {


        profileGridRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        profileGridAdapter = new ProfileGridAdapter(this, filePathList, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                showDialog();

            }
        });

        profileGridRecyclerView.setAdapter(profileGridAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void showDialog() {

        dialogFragment = EJobsDialogs.openGalleryChooser(this);
        dialogFragment.show(getSupportFragmentManager(), TAG_DIALOG);

    }

    public void switchToEditMode() {

        sliderLayout.setVisibility(View.GONE);
        slider.setVisibility(View.INVISIBLE);
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        navBack.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);
        toolbarTitle.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        editSaveText.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        pageIndicator.setVisibility(View.INVISIBLE);
        profileGridRecyclerView.setVisibility(View.VISIBLE);


    }

    public void switchToSaveMode() {

        sliderLayout.setVisibility(View.VISIBLE);
        slider.setVisibility(View.VISIBLE);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        navBack.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        toolbarTitle.setTextColor(ContextCompat.getColor(this, R.color.white));
        editSaveText.setTextColor(ContextCompat.getColor(this, R.color.white));
        pageIndicator.setVisibility(View.VISIBLE);
        profileGridRecyclerView.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                openCamera();
            } else {
                Toast.makeText(ProfileActivity.this, R.string.app_permission_camera, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                Log.v("FileChoose", "Permission: " + permissions[1] + "was " + grantResults[1]);
                openGallery();
            } else {
                Toast.makeText(ProfileActivity.this, R.string.app_permission_gallery, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_GALLERY && resultCode == RESULT_OK) {


            cropImage(data.getData());

        } else if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_CAMERA && resultCode == RESULT_OK) {

            Uri capturedUri = FileProvider.getUriForFile(this, getPackageName() + ".fileProvider", tempFile);
            cropImage(capturedUri);
        } else if (resultCode != RESULT_CANCELED && resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {

            Uri finalUri = UCrop.getOutput(data);
            String finalPath = FileHelper.getPath(this, finalUri);

            File compressedImage = new Compressor.Builder(this)
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .build()
                    .compressToFile(new File(finalPath));
            String compressedImagePath = compressedImage.getAbsolutePath();

            if (!isProvider) {

                filePath = AppConstant.EMPTY_STRING;
                filePath = compressedImagePath;

                setUserProfile("file:///" + filePath);
            } else {

                ProfileFeed profileFeed = new ProfileFeed();
                profileFeed.setFilePath(compressedImagePath);
                profileFeed.setIsAddItem(false);
                filePathList.add(filePathList.size() - 1, profileFeed);
                profileGridAdapter.notifyDataSetChanged();
            }

        }


    }


    public void optionClick(boolean isGallery) {

        dialogFragment.dismiss();

        if (isGallery) {


            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Log.v("Permission", "Permission is granted");
                    openGallery();
                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                }
            } else {
                openGallery();
            }
        } else {

            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    Log.v("Permission", "Permission is granted");

                    openCamera();

                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            } else {

                openCamera();
            }

        }


    }

    public void openCamera() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {

            File file = createTempFile();
            if (file != null) {

                Uri contentUri = FileProvider.getUriForFile(this, getPackageName() + ".fileProvider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                startActivityForResult(intent, RESULT_OPEN_CAMERA);

            }
        }
    }

    public void openGallery() {

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(i, RESULT_OPEN_GALLERY);

    }


    private File createTempFile() {


        String fileName = "IMG_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        tempFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/ejobsImages", fileName);
        File rootDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/ejobsImages");
        if (!rootDir.exists()) {

            rootDir.mkdirs();
        }
        return tempFile;
    }


    private void cropImage(Uri selectedImage) {

        File destinationFile = createTempFile();
        UCrop uCrop = UCrop.of(selectedImage, Uri.fromFile(destinationFile));
        UCrop.Options options = new UCrop.Options();
        options.setToolbarTitle(getString(R.string.editPhoto));
        options.setToolbarColor(ContextCompat.getColor(ProfileActivity.this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(ProfileActivity.this, R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(ProfileActivity.this, R.color.colorPrimaryDark));
        uCrop.withOptions(options).start(this);
    }

    public void setUserProfile(String selectedImage) {

        Glide.with(this).load(TextUtils.isEmpty(selectedImage) ? null : selectedImage)
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(profilePicture);
    }

    @Override
    public void onProfileUploadResponse() {

    }

    @Override
    public void populateCities(CityFeed cityFeed) {
        cityList.clear();
        cityList.addAll(cityFeed.getCityList());
        selectedCityPos = 0;
        List<String> mCityList = new ArrayList<>();
        for (int i=0;i< cityFeed.getCityList().size();i++){
            CityFeed.City city = cityFeed.getCityList().get(i);
            mCityList.add(cityFeed.getCityList().get(i).getName());
            if(user.getCityId().equals(city.getId())){
                selectedCityPos = i;
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mCityList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setOnItemSelectedListener(this);
        citySpinner.setAdapter(adapter);
        citySpinner.setSelection(selectedCityPos);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(cityList.size()>0){
            selectedCityPos = position;
            txtCity.setText(cityList.get(position).getName());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onResponse(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.register_error2), Toast.LENGTH_SHORT).show();
    }

    public void uploadProfilePic(boolean isProvider, List<ProfileFeed> filePathList) {
        if(cityList.size() >0) {
            CityFeed.City city = cityList.get(selectedCityPos);
            String cityId = city.getId();
            if(isProvider)
                presenter.uploadProfile(this, isProvider,null, edName.getText().toString().trim(), cityId, edPhoneNo.getText().toString().trim(), filePathList);
            else
                presenter.uploadProfile(this, isProvider, filePath, edName.getText().toString().trim(), cityId ,edPhoneNo.getText().toString().trim(), null);
        }


    }


    @Override
    public void showProgressDialog() {
        showLoader(getString(R.string.progress_text));
    }

    @Override
    public void hideProgressDialog() {
        hideLoader();
    }

    @Override
    public void showFieldError(int errorCode) {
        switch (errorCode) {
            case 0:
                edName.setError(getString(R.string.required));
                break;
            case 1:
                edPhoneNo.setError(getString(R.string.required));
                break;
        }
    }

}

