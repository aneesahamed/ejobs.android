package com.forsa.ejobs.ui.myRequest;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forsa.ejobs.R;
import com.forsa.ejobs.adapter.MyRequestListAdapter;
import com.forsa.ejobs.common.AppConstant;
import com.forsa.ejobs.common.FragmentListener;
import com.forsa.ejobs.common.OnItemClickListener;
import com.forsa.ejobs.common.Utils;
import com.forsa.ejobs.model.entity.RequestFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class RequestListFragment extends Fragment {

    private FragmentListener fragmentListener;
    Unbinder unbinder;

    @BindView(R.id.requestListRecycler)
    RecyclerView requestList;
    MyRequestListAdapter myRequestListAdapter;
    ArrayList<RequestFeed> requestFeeds = new ArrayList<>();
    Toolbar toolbar;
    public RequestListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_request_list, container, false);
        unbinder = ButterKnife.bind(this,view);
        toolbar = getActivity().findViewById(R.id.toolbar);

        requestList.setLayoutManager(new LinearLayoutManager(getActivity()));
        myRequestListAdapter = new MyRequestListAdapter(getActivity(), requestFeeds, new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                if(fragmentListener!=null){

                 fragmentListener.callNextFragment(object , AppConstant.MY_REQUEST_FRAG);

                }

            }
        });
        requestList.setAdapter(myRequestListAdapter);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        getRequestList();
        Utils.setToolbarTitle(toolbar ,getString(R.string.myRequest));
    }


    public void getRequestList(){

        ArrayList<RequestFeed> rList = new ArrayList<>();

        for(int i=0;i<10;i++){

            RequestFeed requestFeed = new RequestFeed();
            requestFeed.setRequestBudget("200");
            requestFeed.setRequestDescription("Lorem Ipsum Lor Ipsum nting and typesetting industry. Lorem Ipsum has been the industry's standa");
            requestFeed.setRequestOffer("07");
            requestFeed.setRequestDate("10 April 2018");
            rList.add(requestFeed);
        }

        requestFeeds.addAll(rList);
        myRequestListAdapter.notifyDataSetChanged();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }


    @Override
    public void onDestroyView(){

        super.onDestroyView();
        unbinder.unbind();
    }
}
